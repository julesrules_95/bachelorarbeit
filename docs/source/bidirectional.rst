###########################
Bidirectional clustering
###########################

The Bidirectional clustering algorithm originally written by Wiebke Petersen 
and Oliver Hellwig was called from the python project using `rpy2`_.

In this part of the documentation you will learn about the general mechanics 
of this algorithm and how it was modified to be used on the food-corpus.

.. _rpy2: https://rpy2.github.io/doc/latest/html/index.html


*****************************
General mechanics 
*****************************

The bidirectional algorithm takes as input a table of form:

+------+-----------+-------+
| Noun | Adjective | Count |
+======+===========+=======+
| dog  | blue      | 1     |
+------+-----------+-------+

Using this table, a vector space matrix is created having the adjectives 
as rows and the nouns as columns such that each adjective is described by the 
frequencies of nouns it occurs with. 
In an initial thinning step sparse words - indicated by a parameter defining the 
number of times this word cooccurs with nouns/adjectives - are removed from the 
matrix iteratively. Afterwars the matrix is transformed into a binary one by 
substituting all non-zero frequencies by 1. 

Using that binary matrix the pairwise distances between all rows are calculated 
based on the jaccard distances of the rows: 

.. math::
   d_{i,j} = 1 - \frac{|\vec{r_i} \land \vec{r_j}|}{|\vec{r_i} \lor \vec{r_j}|}

To avoid the clustering of nouns that only share a few adjectives a threshold 
is set such that nouns have to share at least that specific number of adjectives 
for their distance to be taken into account.

In the original version of the bidirectional algorithm LDA was also used by 
reweighting the jaccard distances with the distances obtained from the lda. This 
option was not used on the food corpus.

Using this score, the top scoring pairs have high similarities in the binary 
vector space. The new word clusters are now created from the top 5% by contructing 
the transitive closures. The distributional representation of the new cluster is 
built by merging all rows that belong to one transitive closure with a majority 
based binary operator:

.. math::
   \vec{R_k} = \begin{cases}1 & if \sum_{\vec{r}\in R} r_k \geq \frac{|R|}{2}\\0 & 
   else\end{cases}

The rows belonging to the new cluster are now replaced by the row vector :math:`\vec{R}` 
shrinking the matrix dimension by :math:`|R| - 1`. 

The algorithm terminates when no rows are found that have more non-zero columns 
in common than specified in the threshold. Otherwise the matrix is transposed and 
the clustering method is applied to the other word class.


********************************
Modifications for food-corpus
********************************

To apply the bidirectional algorithm on the food corpus the following modifications 
have been made. 

The sparsity parameter has been set to: 

   * articles: 1
   * recipes: 4
   * comments: 5
   * all: 5

As already mentioned the ``USE_LDA`` parameter which indicates whether or not to 
reweight the distance scores using LDA has been set to ``FALSE``.

The parameters regarding the reduction of the matrix have been set to:

   * reduction rate: 0.1
   * reduction factor: 0.05

The threshold for similarity detection using jaccard has been set to:

   * nouns: 
   * adjectives: 

The minimal number of common terms for a pair has been set to ``3``.

*****************************
Postprocessing
*****************************

To be able to visualize and evaluate the results of bidirectional clustering 
efficiently, the results need to be postprocessed. 

First the class names are normalized to integer numbers starting from zero for 
adjectives and from the number of adjective clusters +1 for nouns. Additionally 
a second dataframe is created which stores the hierarchical data - more specifically 
the subclusters - of each clusters for visualization. 

Lastly coordinates are added for each word or subcluster with y=1 and x ascending 
from 1 for each word in the cluster.