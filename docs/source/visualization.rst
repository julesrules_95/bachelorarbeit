#######################
Visualization
#######################

The dashboard providing the visualizations was created using `dash`_ 
and `plotly`_ which are python libraries designed for plotting and 
creating dashboards.

The visualizations are structured like a webpage and divided into 
five parts.

.. _dash: https://dash.plotly.com/
.. _plotly: https://plotly.com/python/


***************************
Home
***************************

This is the starting page if you want to view the visualizations. 
Here you will find a quick overview which parameters were used for 
each part of the project as well as links to the resources and the 
most relevant visualization.


***************************
K-means clustering
***************************

The K-Means visualization can be chosen for each kind of resource being 
either: 

   * all
   * articles
   * comments
   * recipes

For each resource the visualization consists of three major parts.

General visualization of all clusters
======================================

The visualization of all clusters consists of two seperate plots.
On the left side you will see a 3D-scatter plot of all adjectives with 
their respective cluster indicated by their color as well as the same 
plot for all nouns on the right side. 

.. image:: _static/k-means-overview.png
   :width: 600

Here you can simply take a look at the whole structure of the corpus 
when plotted as well as some key insights from the tooltips.

Visualization of a single cluster
======================================

Using this kind of visualization you will see a single cluster you 
can choose using the dropdown-box on the left side shown in a 3D-
scatter plot. On the right side you will see a bar chart showing you 
the most relevant clusters of the other word class based on their 
tf-idf score.

.. image:: _static/k-means-cluster.png
   :width: 600


Visualization of a single word
=====================================

When viewing the visualization of a single word you will see the frequency 
of the word you have chosen in the dropdown-box within its own cluster on 
the left side. On the right side you will see another bar chart which shows 
you the most likely adjectives or nouns for your chosen word to appear with 
based on the tf-idf score of their cluster and their freuqency within their 
own cluster.

.. image:: _static/k-means-word.png
   :width: 600

***************************
Agglomerative clustering
***************************

The visualizations for agglomerative clustering are structured the same way 
as the ones for k-means clustering.


***************************
Bidirectional clustering
***************************

The bidirectional clustering visualizations can be chosen for each kind of 
resource being either: 

   * all
   * articles
   * comments
   * recipes

These visualizations are fairly simple due to the fact that no word-vectors 
were used. For that reason they consist of only one page being divided into 
two parts.

.. image:: _static/bidi-vis.png
   :width: 600

Visualization based on a chosen cluster
=========================================

On the left side you will be able to choose a cluster based on its name e.g. 
``Cluster 100`` and below it you can take a look at a scatter plot which 
shows you each word included in this cluster as a point in the plot.

Due to the lack of word vectors or anything similar, the coordinates of each 
word have been set (artibrarily) to ``1`` on the y-axis and ascending from ``1`` 
on the x-axis depending of the number of words in the cluster.

Therefore the plot does not convey any additional information apart from showing 
you which words have been assigned to the given cluster. 


Visualization based on a chosen word
=========================================

On the right side, you can choose a word to see the cluster it was assigned to. 
The plot you see is exactly the same as the plot on the left given you choose the 
same cluster. 

Therefore this plot only gives you the chance to see the results of a single word 
without knowing which cluster it was assigned to. 


***************************
Evaluation
***************************

The results of the evaluation can be chosen for each kind of resource 
being either: 

   * all
   * articles
   * comments
   * recipes

Each of these visualizations consists of two 'pages'. 

Overview over evaluation results 
==================================

On the left side you can see a bar chart comparing the metric scores 
of each clustering algorithm. On the right side you can see a grouped 
bar chart showing you the total scores per algorithm of: 

   * true positives
   * true negatives 
   * false positives
   * false negatives 

.. image:: _static/eval-overview.png
   :width: 600

Individual results for each word class
=======================================

The visualization of the individual evaluation results for each word class 
consists of the same two parts as the overview but shown for only 
adjectives at the top and only nouns at the bottom of the page.