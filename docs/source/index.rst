.. bachelor-thesis documentation master file, created by
   sphinx-quickstart on Thu Aug  6 06:45:52 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to bachelor-thesis's documentation!
===========================================

This documentation was created to give further and more detailed insights in 
how the project and it's individual components work. 

It can be seen as an alternative version of the methodological part of the 
bachelor-thesis as it contains all the details about the code, which the 
thesis itself covers only on a superficial level.

As of now, the corpus consists of four different types of texts - namely the 
recipes, comments on the recipes, articles and the combined corpus - leading 
to the following quantities of noun-adjective pairs:

1. articles: 8.169 pairs
2. comments: 706.667 pairs
3. recipes: 706.679 pairs

This leads to a total of 1.423.998 adjective noun pairs from the food-related 
corpus.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   usage
   downloading
   cleaning
   kmeans
   agglomerative
   bidirectional
   evaluation
   visualization
   code



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
