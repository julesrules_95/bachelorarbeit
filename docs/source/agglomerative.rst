##############################
Agglomerative clustering 
##############################

To be able to compare the bidirectional clustering algorithm not 
only to kmeans clustering but also to a second - "classic" - clustering 
approach agglomerative clustering was used.

***********************************************
Difference agglomerative vs kmeans clustering
***********************************************

Similarities in usage
-----------------------

The pre- and postprocessing of agglomerative and kmeans clustering was 
the same. This means in detail that the same functions having the same 
parameters were used for: 

   * pre-processing:
      * getting the data into the right format
      * word2vec
      * dimensionality reduction
   * post-processing: 
      * preparing the counts per cluster
      * adding the score of representativeness of a word in its cluster
      * getting tf-idf scores 

Differences of algorithms
----------------------------

This means that the only difference in obtaining results lies within the 
actual clustering algorithm: 

The agglomaerative clustering algorithm is the most commonly known 
hierarchical clustering algorithm. It starts by assigning each datapoint 
to a cluster of its own and from there on merging clusters that are considered 
to be similar. Based on its working machanics it is considered a bottom 
up approach. 

As this algorithm provides us with the possibility of not having to determine 
the k best clusters in advance but finding them using a distance-threshold it 
is a decent choice for a second algorithm to be evaluated on the data set. 

Parameters used
------------------

The parameters used for agglomerative clustering were the following ones: 
   * affinity: euclidean
   * linkage: average
   * distance theshold: 
      * all: 2
      * articles: 25
      * comments: 2
      * recipes: 2