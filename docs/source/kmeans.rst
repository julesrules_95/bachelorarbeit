#######################
K-means clustering
#######################

To be able to cluster the food related corpus using kmeans a few steps had 
been used. The first one - which is not included in this part of the docs - 
is the cleaning of the corpus. 

All following steps are explained in this part of the documentation.


**************************
Getting wordvectors 
**************************

As we want to cluster words, we need to vectorize them to be able to 
do so. This is done in the following steps.

Preparing the pairs
======================

To be able to vectorize the words, they need to be in a specific format. As 
we want to look into noun-adjective pairs the most reasonable approach was to 
take the ``X-pairs-cleaned.txt``, remove all the pos-tags and store them in a 
separate dict called ``pos_dict`` which maps from a word to its pos-tag. 

This approach was chosen because **word2vec** which is used later on uses 
bigrams to vectorize a word and as we want a noun to be "defined" by its 
cooccuring adjectives and and vice versa, it is most useful to use the already 
existing pairs as bigrams (or n-grams) to achieve that.

Applying word2vec
======================

To apply word2vec now a word2vec model is instanced using the parameters given 
to the ``separate_word2vec()`` function and the previously prepared pairs to train 
the model on. After that the corpus is split into a noun-corpus and an 
adjective-corpus using the ``pos_dict``.

The default parameters doing this are:
   * size: 100
   * window: 5


**************************
Clustering the vectors
**************************

Now for each dataset (the nouns and the adjectives) the kmeans algorithm is run 
separately. 

Reducing dimensionality
==========================

This step can be fairly optional but as we also want to visualize our clusters, 
we need to reduce the dimensionality of the word vectors. 

This is done using TSNE with the following parameters as default:
   * n_components: 2
   * perplexity: 30
   * n_iter: 1000

Finding the best number of clusters
=======================================

Now the best number of clusters has to be found. To do so ``MiniBatchKMeans`` 
is used which is faster than ``KMeans`` while yielding only very slightly 
different results. To find the best k a list of possible values is given and 
the corresponding scores are stored in lists. 

At the moment the scores used to find the best model are: 
   * inertia 
   * coherence 
   * Calinski and Harabasz score
   * Davies-Bouldin score

If the functions runs in supervised mode, the results will be plotted against 
each other and the user is asked to choose the value of the best k. If not in 
supervised mode, the scores will be normalized onto a scale of 0 to 5 and the 
k delivering the highest merged score using the formula :math:`score_{coherence}+
score_{Calinski\_and\_Habarasz}-score_{inertia}-score_{Davies\_Buildin}` is chosen 
as ``best_k``.

Afterwards a ``KMeans`` model using the best k that has been found is instanced, 
trained on the data and returned as well as the value of ``best_k``.

Applying and combining results
=======================================

Now the already trained KMeans-model is used to predict the clusters for the 
datapoints as well as the centers of each cluster. Then, the words (vocab), the 
x- and y-coordinates and the corresponding cluster are stored in a dataframe. 

+-------+---------------+---------------+---------------+--------------------+
| index | x             | y             | z             | cluster            |
+=======+===============+===============+===============+====================+
| vocab | x-coordinates | y-coordinates | z-coordinates | predicted clusters |
+-------+---------------+---------------+---------------+--------------------+

In a second dataframe the cluster-names and their centers are stored.

+----------+----------------------+----------------------+----------------------+
| index    | x                    | y                    | z                    |
+==========+======================+======================+======================+
| clusters | center-x-coordinates | center-y-coordinates | center-z-coordinates |
+----------+----------------------+----------------------+----------------------+


************************************
Adding frequency and tf-idf scores 
************************************

After doing the "raw" kmeans clustering, the freuency of a word in its own 
cluster and its score is also added to the dataframe containing the results.

After all that is done, the dataframes and dictionaries are saved (as precomputed 
files) to speed up evaluation and visualization. 

Preparing counts
==================

To be able to use tf-idf later on, the ``X-counts-cleaned.csv`` is loaded 
into a dataframe called ``data_tfidf`` and transformed in a way such that it 
displays the counts per cluster instead of the counts per word. Whilst doing 
that, the freuqency of a word in its own cluster is being stored in a dictionary 
called ``cluster_dict`` which maps from the name of a cluster to a dictionary 
mapping from a word to its frequency.


Adding frequencies and scores
==============================

Now, the relative frequency of each word is stored in the column ``frequency`` 
in the respective dataframe using the formula :math:`\frac{occurrences_{word}}
{n_{words\_in\_cluster}}`.

To get the *score* of a word, also the euclidean distance to the center of its 
cluster is used as we suppose that a word is a better representative of its own 
cluster the closer it is to the center of the cluster (not an outlier). 
Therefore, and to take the frequency into account, the euclidean distance is 
normalized and used as a factor on the freuency:

.. math::

   frequency \cdot \frac{(1+max_{euclidean\_distance})-euclidean\_distance}
   {1+max_{euclidean\_distance}}

This formula yields a factor for the euclidean distance which is 1 if the word is 
placed at the same coordinates as the cluster's center and gets smaller the more  
distant a word is from the center with the minimum value being :math:`\frac{1}
{1+max_{euclidean\_distance}}`.

The frequency and score are added to the respective dataframe of nouns and 
adjectives: 

+-------+---------------+---------------+---------------+--------------------+----------------------+------------------+
| index | x             | y             | z             | cluster            | frequency            | score            |
+=======+===============+===============+===============+====================+======================+==================+
| vocab | x-coordinates | y-coordinates | z-coordinates | predicted clusters | frequency in cluster | score in cluster |
+-------+---------------+---------------+---------------+--------------------+----------------------+------------------+

The resulting dataframes for both adjectives and nouns are saved as ``df_a.csv`` 
and ``df_n.csv``. The metadata needed later on for evaluation and visualization is 
stored in a dict containing ``{'kind': kind, 'best_k_a': best_k_a, 'best_k_n': best_k_n}``
and saved as ``dict.json``.

Adding tf-idf scores
======================

Lastly the counts which have been stored in the ``data_tfidf`` dataframe are 
transformed to tf-idf counts using the TfidfTransformer from sklearn. The resulting 
array is once again used as a pandas dataframe and stored as ``df_tfidf.csv``.