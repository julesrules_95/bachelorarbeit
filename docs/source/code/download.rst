###########################
Code for downloading
###########################

.. automodule:: bathesis.download
   :members:

   .. autoclass:: Resource
      :members: