#######################
Downloading
#######################

To be able to establish a corpus of sufficient size, recipes 
were downloaded from multiple sources. These are:

* `Chefkoch`_
* `Kochbar`_
* `Küchengötter`_
* `DasKochRezept`_
* `Kaufland`_

.. _Chefkoch: https://www.chefkoch.de/
.. _Kochbar: https://www.kochbar.de/
.. _Küchengötter: https://www.kuechengoetter.de/
.. _DasKochRezept: https://www.daskochrezept.de/
.. _Kaufland: https://www.kaufland.de/rezepte/rezeptsuche.html

The basic idea behind downloading all the recipes is to iterate over 
the pages containing recipes and retrieving their links via ``requests`` and 
``BeautifulSoup`` while changing the url for the next page using a regex.

Afterwards all links that could be found are scoured for the actual recipe they 
contain which is being extracted and saved to a file.


.. warning::
   The scraping for links and downloading can take a vast amount of time as a 
   timer is used to prevent spamming sites such that they become non-respondant. 
   Added to that, based on your system's set-up the scraping may get stuck. This 
   hasn't happened on a 2018 Mac, but on another system the downloading has been 
   tested on (stuck after approx. 250k recipes).

**************************
Getting the links
**************************

To be able to do so a few prerequisites had to be taken. First it had to be 
stored whether a resource contains all recipes on one site - so you just need 
to update the parameters to scroll through - or if it is structured into categories. 
This information is hardcoded in the keys of the ``category_identifiers`` dict. 

Then, the ``get_all_recipe_links()`` function iterates over all given resouces and 
initializes a Resource-object for each of them. Each Resource-object has a few attributes. 
The relevant ones for getting the links are:

* category_identifier: either the identifier or ``None`` if the resource does not have any 
  categories
* link_identifier: identifies the the link
* page_init: the suffix for the url of the first page
* page_ind: a regex to iterate through pages
* basic_url: the basic url of the website to append specific suffixes to

Based on these attributes, the function iterates over all possible pages and searches for any 
links on them. After finding all possible links, they are saved in a json-file to avoid frequent 
scraping for links and make it possible to update only a specific resource.

The steps to extract all links are the following:


Getting category-links
==========================

The concept behind getting the links of categories is fairly simple. As all resources 
that are categorized also have a page containing links to the categories, this was 
used to get the links. 

To be able to do so, the identifier for each resource was mapped onto an id for the 
resource itself in a dict called ``category-identifiers``. Using that, the page was 
searched for any elements of that type having a specific class name, which then are 
searched again for any links they might contain. These links are extracted and stored 
in a dict which maps from the id of the resource to a list of the links of categories.


Scrolling through pages
==========================

To scroll through pages at first the url given in the source parameter is searched for 
links to recipes, then the url is extended with the ``page_initializer``, which contains 
all the parameters needed for scrolling, and gets incremented based on the 
``page_indicator``. This process stops as soon as there were no recipes to be found on 
the page. Then, all the links that could be retrieved are given back in a list. 


Getting recipe-links
==========================

To get all the recipe-links from a page, this page is - analogous to finding categories - 
searched for identifiers as stored in the ``recipe-identifiers`` dict and these are then 
searched again for any links they might contain. 
All the links that could be found are given back in a list. 


**************************
Downloading the recipes
**************************

To download the textdata of all the relevant pages, it has to be determined whether the resource 
contains articles or recipe and possibly comments. To do so, first there are a few relevant 
attributes of each Resource-object: 

* textdata_identifier: indicates how to search for the relevant texts of the recipe/article
* comment_id: identifies the position of the comment 
* comment_text: indicates how to get the text of a comment

The ``download()`` function itself uses only a few - pretty straight forward - steps. 
First it tries to load already existing json files of the ``article_texts``, ``comment_texts`` 
and ``recipe_texts`` dicts as storing all found textdata in them enables the project to 
update single resources without losing the rest of the data. If there is an error while 
loading the json-file, a new dict with all the keys of the resources but ``None`` as value 
is initialized. 
Then it tries to load the ``links.json`` file. If there is an error doing that all links 
for the sources given are being retrieved using the ``get_all_recipe_links()`` function.

Now that all needed resources are found, it simply iterates over each of the sources given 
and initializes a Resource-object for each of them and loops over all links for this resource 
to download their textdata based on the identifiers in the object's attributes.
After finding all possible data for an object, the texdata is added to the corresponding dict 
and saved as a text-file as well as a json-file.