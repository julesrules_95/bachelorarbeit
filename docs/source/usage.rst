#######################
Usage
#######################

When using the bachelor-thesis project you have two options: using only 
the interactive visualizations part or also using the development options.

***************************
Usage as 'user'
***************************

If you only want to use the user-interface simply type::

   poetry run vis

This will  start up the dash app which gives you visualizations and the 
results of the evaluation of different clustering algorithms based on the corpus. 

.. note:: 
   If you want to choose different parameters for the k-means clustering you can do 
   so in the ``kmeans.py`` or ``config.R`` and by re-running both algorithms. But as 
   extensive clustering using a vast number of k's was used to find the best k, 
   the default evaluation and graphs will be based on the best parameters found.

***************************
Usage as developer
***************************

However, maybe you don't want to simply see the results but also change something 
about the corpus or add new data-sources etc. etc. Therefore this part of the docs 
explains how to use the bachelor-thesis's command line interface. 

.. warning:: 
   This project was developed and tested on unix. However, when it was run on windows 
   some functions and libraries did not work properly. Therefore, if you want to 
   use the functions to download and analyze the data, you'll want to use a unix 
   based operating system.

In general you have a few commands you can use. These are::

   bathesis updatelinks
   bathesis download
   bathesis pair
   bathesis count
   bathesis clean
   bathesis kmeans
   bathesis aggl
   bathesis bidirectional
   bathesis gold
   bathesis evaluate
   bathesis visualize
   bathesis zip
   bathesis unzip

Each of these also has a few subcommands. 


Update links
==========================

Via the ``updatelinks`` command you can update the links to the texts and choose the source 
you want to update links for::

   bathesis updatelinks [-h] [-s,--source sources] 

Optional arguments:

   :-s, --source: The id of resources you want to get links from
   :--startnew: Start updating links from blank dict.


Download
==========================

Via the ``download`` command you can download all the textdata for your sources::

   bathesis download [-h] [-s,--source sources] [--startnew]

Optional arguments:

   :-s, --source: The id of resources you want to download from
   :--startnew: Start downloading the resource anew.


Pair
==========================

Via the ``pair`` command all adjective-noun pairs will be extracted::

   bathesis pair [-h] [-s,--source sources]

Optional arguments:

   :-s, --source: The id of txt-files you want to extract pairs from


Count
==========================

Via the ``count`` command you can count the frequency of noun-adjective pairs::

   bathesis count [-h] [-s,--source sources]

Optional arguments:

   :-s, --source: The id of resources for which you want to count the pairs


Clean
==========================

Via the ``clean`` command you remove all the words which are less frequent in 
the food corpus than in the reference corpus and thus less representative of it::

   bathesis clean [-h] [-s,--source sources]

Optional arguments:

   :-s, --source: The id of resources for which you want to count the pairs


Kmeans
=========================

Via the ``kmeans`` command you can perform kmeans clustering on your data - 
given it is already paired and counted - and store the results in the static/clustered/kmeans 
folder for evaluation and visualization::

   bathesis kmeans [-h] [-s,--source sources] [--extensive] [--supervised]

Optional arguments:

   :-s, --source: The id of resources you want to perform kmeans on.

   :--extensive: Choose to do extensive clustering by using a range of numbers as possible n_cluster.

   :--supervised: Choose the best number of clusters yourself instead of having it automatically chosen for you.


Agglomerative clustering
=========================

Via the ``aggl`` command you can apply the agglomerativr clustering algorithm on your data - 
given it is already paired and counted - and store the results in the static/clustering/aggl/  
folder for evaluation and visualization::

   bathesis aggl [-h] source

Positional arguments:

  :source: The id of the resource you want to perform bidirectional clustering on.


Bidirectional clustering
=========================

Via the ``bidirectional`` command you can apply the bidirectional algorithm on your data - 
given it is already paired and counted - and store the results in the bidirectional/data/result  
folder for evaluation and visualization::

   bathesis bidirectional [-h] source

Positional arguments:

  :source: The id of the resource you want to perform bidirectional clustering on.


Prepare gold standard 
=========================

Using the ``gold`` command the given csv's denoting similar adjectives and nouns 
will be used to create a list of pairs of words which is needed for evaluation::

   bathesis gold [-h]


Evaluating
=========================

Using the ``evaluate`` command you the results of the clustering algorithms will 
be evaluated against the gold standard::

   bathesis evaluate [-h]

.. warning:: 
   This is done once for all resources and algorithms which means there will be an 
   error if a resource is missing. Yet an option to choose can be easily implemented 
   if needed.


Zip
=========================

Zip up all the static resources to reduce space consumption and especially to 
simplify pushing to and cloning from a repo::

   bathesis zip [-h]

Unzip
=========================

Unzip all the zipfiles you have cloned or downloaded from the repo::

   bathesis unzip [-h]