Bachelor-thesis code documentation
===========================================

Here you will find all the detailed documentation for the code. 

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   code/download
   code/clean
   code/kmeans
   code/agglomerative
   code/bidirectional
   code/evaluation
   code/visualization
   code/zipping