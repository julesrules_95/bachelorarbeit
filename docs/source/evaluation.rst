#######################
Evaluation
#######################

The evaluation of the algorithms consists of three major parts 
being: 

   * the creation of a dataset to develop a gold-standard on
   * evaluating the results of a survey to develop the gold-standard
   * calculating the rand index

***********************************
Creating csv for gold standard 
***********************************

To create a gold-standard a few requirements had to be met: 

   #. the chosen words need to be included in the results of each algorithm 
   #. we want the gold standard to be based off of human intuition: 

      a. it should show which nouns and which adjectives belong together
      b. more specifically it should determine noun/noun and adj/adj correlations

   #. to ensure 2. we only want the most prevalent words 
   #. we need an appropriate number of words to use 

To ensure all these points, the function that creates the csv's for the gold-
standard iterates over all resources. For each of these the results of the 
'classic' clustering approach, the results of bidirectional clustering and the 
``X-counts-cleaned.csv`` (where X is the id of the resource) are used. 

First df's containing the sums of cooccurences for each word are calculated based 
on the ``df_counts``. Then both the ``df_classic`` and the ``df_bidi`` are used 
to strip the ``df_counts`` from words which are not included in both. From the 
remaining words only the 50 most frequent ones (based on the ``df_counts``) are 
used to create the gold standard. 

While this function iterates over each resource, it updates a set of adjectives 
and nouns by adding the 50 most frequent per resource. Based on these sets the 
csv's to create the gold standard are created. 


*****************************************
Evaluate filled csv's for gold-standard
*****************************************

As we have chosen the rand index as our evaluation metric in advance and we know 
it to operate pairwise, it was the easiest approach to create a list of all the 
pairs of words which were marked as similar - thus belonging to the same cluster - 
based on human intuition. 

To do so the csv containing the results was loaded into a dataframe and in the 
first step stripped from all rows and columns containing only zeros to speed up 
the computation. Then the function iterates over all columns that are left creating 
a temporary dataframe containing only the rows containing not zero. All these row 
names are then appended to the gold-list as pairs of [row, column]. 

This list of pairs is created for both adjectives and nouns indivisually and has 
the format ``[[row, col],[row, col],...]``.


***********************************
Apply rand index
***********************************

To ensure that we obtain valuable results by applying the rand index on our results 
we need to make sure that we operate on the same words for each resource. This is 
achieved by taking the list of nouns/adjectives used for the gold-standard and creating 
a list of nouns/adjectives included the results of every clustering method for each 
resource. These lists of words to be evaluated are stored in a dict to be accessed 
later on. 

In a second step the function iterates over each clustering method for each resource 
and determines based on the list of words to be evalued the number of the following 
groups: 

   * true positive: words correctly assigned to the same cluster
   * true negative: words correctly assigned to different clusters 
   * false positive: words falsely assigned to the same cluster
   * false negative: words false assigned to different clusters

The sum of these groups has to be :math:`{len(words) \choose 2}` as we need to take 
every possible combination of two words into account. 

To compute these groups most efficiently at first ``fp`` (false positive) was set 
to the sum of all combinations of two or more words in the same cluster (in the results). 
The ``tn`` (true negative) has been set to :math:`{len(words) \choose 2}-tn`. Then the 
function iterates over the gold-list and if the pair was also assigned to the same cluster 
in the results, ``tp`` gets incremented while ``fp`` is decremented; otherwise ``fn`` is 
incremented while ``tn`` is decremented. 

Lastly the rand index :math:`\frac{tp+tn}{tp+tn+fp+fn}` is calculated and the total scores 
as well as the rand index are saved to ``static/evaluation/X`` where X is the resource 
evaluated. 