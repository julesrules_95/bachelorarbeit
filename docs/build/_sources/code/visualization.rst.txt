###########################
Code for visualizations
###########################

.. automodule:: bathesis.app
   :members:

.. automodule:: bathesis.app.app
   :members:

*******************************
Functions in index.py
*******************************

.. automodule:: bathesis.app.index
   :members:

.. automodule:: bathesis.app.navbar
   :members:

***********************************
Functions for home visualization
***********************************

.. automodule:: bathesis.app.vis.home
   :members:

******************************************************************
Functions for kmeans and agglomerative clustering visualizations
******************************************************************

.. automodule:: bathesis.app.vis.clustering
   :members:

*********************************************
Functions for bidirectional visualizations
*********************************************

.. automodule:: bathesis.app.vis.bidirectional
   :members: