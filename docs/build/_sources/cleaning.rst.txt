#######################
Cleaning
#######################

In this part of the documentation you will learn about how the text 
scraped from all resources was preprocessed to find all noun-adjective verbs.

To be able to identify the most relevant pairs for the corpus regarding food 
related topics, a reference corpus was needed. For that reason the ``merged.dat`` 
from the original bidirectional clustering project was used.

The main library that was used for POS-tagging was `SpaCy`_ as it is a 
state-of-the-art natural language processing library which comes with fast parsing 
and great accuracy as well as german models to use. To lemmatize the tagged words 
the `GermaLemma`_ library was used as it yields even better results in lemmatizing 
than SpaCy.

.. _SpaCy: https://spacy.io/
.. _GermaLemma: https://pypi.org/project/germalemma/


*********************************
Preparing the reference counts
*********************************

Whenever the reference counts are needed, they are constructed from the ``merged.dat`` 
file in the bidirectional/data/permanent folder. To do so the lines consisting of three 
columns: 

+------+-----------+-------+
| Noun | Adjective | Count |
+======+===========+=======+
| dog  | blue      | 1     |
+------+-----------+-------+

Are read into a list of form ``[[col1, col2, col3],[col1,col2,col3],...]`` called ``refs``. 
From that list the set of nouns and adjectives is constructed which are then used to 
construct a numpy-array of shape (number_nouns, number_adjectives). Lastly the function 
iterates over ``refs`` and adds the counts to the array before returning it.


******************************
Getting noun-adjective pairs
******************************

To get the pairs of nouns and adjectives in a text, SpaCy was used. At first, the txt-file 
had to be splitted into chunks of at most one million words (due to the memory usage of the 
SpaCy parser). Then for each text-chunk the noun-chunks were retrieved, which is fairly simple 
in SpaCy as it offers a ``noun_chunks`` attribute for your document as a part of its dependency 
parsing functionality. 

.. warning:: As for now there is still a relatively high chance that some words are falsely tagged 
   as being an adjective or noun in spite of being neither of those. Therefore the analysis will still 
   be laggy to some point until a better pos-tagger is used.

Now, for each chunk, a specific pattern consisting of at least one adjective and the noun was 
matched (also using a SpaCy module) and each of the words in a pattern is normalied by:

   1. removing all unnecessary punctuation being:
      a. every character at the beginning/end of a word being not a letter or a number
      b. all ``",`,´,'``
   2. reducing all compositions to the last word in them as it is most likely the most 
      relevant one 

Afterwards the lemmas of these words being retrieved using germalemma as well as their POS-tags 
from spacy are stored in a list of lists of tuples looking like this: 
``[[('schön', 'ADJ'), ('Haus', 'NOUN')], [('alt', 'ADJ'), ('süß', 'ADJ'), ('Katze', 'NOUN')]``


**********************************
Updating all-pairs.txt
**********************************

To enable the user to update the all-pairs.txt which contains all noun-adjective pairs that 
have been found in the food-related resources, there exists a function which first checks if 
a txt-file containing pairs of the corresponding resource exists. Afterwards if combines 
the lists of pairs (as explained earlier) into one and saves it as ``all-pairs.txt``.


**********************************
Counting noun-adjective pairs
**********************************

To count all the found pairs, the ``count_pairs()`` function iterates over a list of ids given 
to it and creates a pandas-dataframe for each source having the nouns as indices and the adjectives 
as columns. Initially each cell in this df is set to 0 and incremented whenever a matching pair is 
found in the document.
Afterwards the df is saved as a csv-file. 


**********************************************
Summing up - Results of pairing and counting 
**********************************************

Getting pairs
=======================================

As of now, the corpus consists of four different types of texts - namely the 
recipes, comments on the recipes, articles and the reference corpus - leading 
to the following quantities of noun-adjective pairs:

1. articles: 11.088 pairs
2. comments: 800.505 pairs
3. recipes: 750.069 pairs

This leads to a total of 1.561.662 adjective noun pairs from the food-related 
corpus as well as 53.437 pairs in the reference corpus.

Counting the pairs -- uncleaned
=======================================

While counting the pairs and creating a dataframe containing nouns, adjective 
and counts of the number of times of their cooccurrence the following results 
have been found: 

1. articles: 3593 nouns and 2025 adjectives
2. comments: 49.011 nouns and 19.250 adjectives
3. recipes: 23.944 nouns and 8.216 adjectives

By combining all food-related resources a total of 63.037 nouns and 22.893 adjectives 
have been found.

.. note:: The number of nouns and adjectives mentioned is not the total number but 
   the number of *unique* nouns and adjectives that cooccur in a noun-phrase and therefore 
   must be assumed to modify each other. More specifically: only the noun-adjective pairs 
   where the noun is modified by the adjective, are counted. 


************************************
Cleaning the corpus
************************************

To efficiently clean the corpus of unwanted words like for example words that are 
incorrectly tagged as noun or adjective whilst being neither of those or simply 
reduce its size, mulitple approaches are possible. 

1. count the occurrences of a noun or adjective in the whole corpus and the 
   reference corpus (the ``tiger_corpus``); if the word is less frequent in 
   the resource than in the reference corpus (by relative frequency) it is not 
   as relevant as we primarily look for pairs that represent a food-related 
   corpus. --> brute force 
2. simplify idea 1: only count the number of occurrences using the already counted 
   pairs; only keep those nouns and adjectives that occur in modified noun phrases 
   more frequently than in the reference corpus (relative frequency) --> easier to 
   compute, yet only takes *modified* noun phrases into account
3. naive idea: use the sole frequency without a reference corpus; only leave the X 
   most frequent words --> does not regard the relevance of a term for the kind of 
   corpus as e.g. ``small`` will always be more frequent than ``whipped`` 

For performance reasons, number 2 was chosen.

Cleaning using relative frequencies
=======================================

The idea behind this was pretty much straight forward: At first the 
``/bidirectional/data/permanent/merged.dat`` is loaded into a df using pandas. Then, the 
function iterates over a list of resources it is given as a parameter. For each of this it 
first loads the ``X-counts.csv`` and then iterates over the reference-dataframe called 
``df_reference``.

To clean up nouns it compares for each row if the index is also an index in the dataframe of 
the resource (called ``df_counts``) and if the relative frequency of this noun (as the indices 
are nouns and the column-names are adjectives) is higher in the reference corpus than in the 
resource; if so, the row is dropped from the ``df_counts``. 
To compare the relative frequency of a noun, the row is summed up and this sum is being divided 
by the number of rows - being the number of unique nouns - in the dataframe:

.. math::
   rel.freq_n = \frac{\sum_{a \in A} count_a}{|N|} 

To clean up adjectives, the same technique is applied, but now the function iterates over the 
columns and compares for each one if the label is also a column label in the ``df_counts`` and 
whether the adjective is more frequent - in terms of relative frequency - in the reference 
corpus than in that of the resource. If so, the column is dropped from the ``df_counts``.

After cleaning all words from the resource that are less frequent - and therefore probably less 
rlevant - there than in the reference corpus, the ``df_counts`` is saved in the ``static/cleaned`` 
folder as ``X-cleaned.csv``. 