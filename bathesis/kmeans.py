import json
import pandas as pd

from bathesis.visualizations import plot_find_best_k

from time import time
from pathlib import Path
from gensim.models import Word2Vec
from scipy.spatial import distance

from sklearn import metrics
from sklearn.cluster import KMeans, MiniBatchKMeans
from sklearn.manifold import TSNE
from sklearn.feature_extraction.text import TfidfTransformer


def get_clean_data(id):
    """
    loads the pairs from the ``X-pairs.txt``, then adds each pair - 
    without pos-tag - as a list to a list of pairs e.g. ``[['klein', 
    'katze'], ['alt', 'hund']]``. Additionally stores the pos-tag of each 
    word in a dictionary e.g. ``{'klein':'ADJ', 'katze':'N'}``

    Used to prepare the data for word2vec.

    :param id: the id of the resource to get data from

    :returns:
       tuple containing two items:
          * data: the list of pairs
          * pos_dict: the dict containing the pos-tag of each word
    """
    with open(str(Path(__file__).parent) + '/static/cleaned/{}-pairs-cleaned.txt'.format(id)) as f:
        pairs = json.loads(f.read())

    data = []
    pos_dict = {}

    # make list of lists -- only leave lemma from a tuple
    for pair in pairs:
        pa = []
        for p in pair:
            pa.append(p[0])
            pos_dict[p[0]] = p[1]
        data.append(pa)

    return data, pos_dict

def prepare_counts(id, cluster_a, cluster_n, cluster_dict, best_k):
    """
    takes the id of a resource and a df containing the clusters for each 
    word; then updates the df containing the counts to counts per cluster 
    instead of counts per word to enable deriving relative probabilities;
    additionally saves the relative frequency of each word in a cluster in 
    a dict mapping from cluster-names to a dict mapping from words to relative 
    frequencies.

    :param id: the resource to be prepared
    :param cluster_a: the df of adjectives including their assigned cluster
    :param cluster_n: the df of nouns including their assigned cluster
    :param cluster_dict: a dict containing the names of the clusters mapping 
       to a default value ``0``
    :param best_k: the best number of clusters to perform k-means on

    :returns:
       tuple containing two items:
          * df: the dataframe containing the counts per cluster
          * cluster_dict: a dict containing the relative frequency of each 
            word per cluster
    """
    with open(str(Path(__file__).parent) + '/static/cleaned/{}-counts-cleaned.csv'.format(id)) as f:
        df = pd.read_csv(f, index_col=0)

    indices = [i for i in df.index if i in cluster_n.index]
    columns = [i for i in df.columns if i in cluster_a.index]

    df = df.loc[indices]
    df = df[columns]

    for n in range(best_k):
        tmp_a = [i for i in cluster_a.index if cluster_a.cluster[i]==n]
        tmp_n = [i for i in cluster_n.index if cluster_n.cluster[i]==n]
        tmp_rows = {i:n for i in df.index if i in tmp_n}
        tmp_cols = {i:n for i in df.columns if i in tmp_a}

        if n in list(cluster_a.cluster):
            df_sum = df.sum(axis = 0, skipna = True)
            div = sum(df_sum[tmp_a])
            cluster_dict[n] = {e:float("{:.5f}".format(df_sum[e]/div)) for e in tmp_a}
        elif n in list(cluster_n.cluster):
            df_sum = df.sum(axis = 1, skipna = True)
            div = sum(df_sum[tmp_n])
            cluster_dict[n] = {e:float("{:.5f}".format(df_sum[e]/div)) for e in tmp_n}

        df.rename(index=tmp_rows, inplace=True)
        df.rename(columns=tmp_cols, inplace=True)

    # combine rows
    df = df.groupby(df.index).agg('sum')
    # transpose to easily combine columns of same cluster
    df = df.transpose()
    df = df.groupby(df.index).agg('sum')
    # transpose back to original shape
    df = df.transpose()
        
    return df, cluster_dict

def separate_word2vec(data, pos_dict, size=300, alpha=0.025, window=5, min_count=1, sample=0.001, workers=3, sg=0, seed=1234, hs=0, negative=5, ns_exponent=0.75, cbow_mean=1, iter=5):
    """
    trains a word2vec model on the data, then retrieves the wordvectors and 
    corresponding vocab for each word class

    :param data: the data for the model to be trained on
    :param pos_dict: the dict containing the pos-tag for each word
    :param size: optional size of the wordvectors
    :param aplha: optional initial learning rate
    :param window: optional maximum distance between the current and predicted word within a sentence
    :param min_count: optional ignores all words with total frequency lower than this
    :param sample: optional threshold for configuring which higher-frequency words are randomly 
       downsampled, useful range is (0, 1e-5)
    :param workers: optional number of worker threads to train the model
    :param sg: optional training algorithm: 1 for skip-gram; 0 for CBOW
    :param seed: optional, used to make word2vec deterministic
    :param hs: optional, if 1, hierarchical softmax will be used for model training. If 0, 
       and negative is non-zero, negative sampling will be used.
    :param negative: optional, if > 0, negative sampling will be used, the int for negative specifies 
       how many “noise words” should be drawn (usually between 5-20). If set to 0, no negative sampling 
       is used.
    :param ns_exponent: exponent used to shape the negative sampling distribution. A value of 1.0 samples 
       exactly in proportion to the frequencies, 0.0 samples all words equally, while a negative value 
       samples low-frequency words more than high-frequency words
    :param cbow_mean: optional, if 0, use the sum of the context word vectors. If 1, use the mean, only 
       applies when cbow is used
    :param iter: optional, number of iterations (epochs) over the corpus

    :returns:
       tuple containing 4 items:
          * vocab_n: the vocab of the nouns
          * tokens_n: the wordvectors of the nouns
          * vocab_a: the vocab of the adjectives
          * tokens_a: the wordvectors of the adjectives
    """
    model = Word2Vec(data, size=size, alpha=0.025, window=3, min_count=min_count, workers=3, 
        sg=0, sample=0.001, seed=seed, hs=0, negative=5, ns_exponent=0.25, 
        cbow_mean=0, iter=10)

    vocab = list(model.wv.vocab)
    vocab_n = [i for i in vocab if pos_dict[i]=='N']
    vocab_a = [i for i in vocab if pos_dict[i]=='ADJ']
    tokens_n = model.wv[vocab_n]
    tokens_a = model.wv[vocab_a]
    del model

    return vocab_n, tokens_n, vocab_a, tokens_a

def apply_tsne(X, n_components=2, perplexity=30, n_iter=1000, random_state=23):
    """
    initializes a TSNE model based on given parameters, trains it on the given 
    data and reduces the dimensionality of the data

    :param X: the data to be trained on and transformed
    :param n_components: optional number of dimensions desired
    :param perplexity: optional
    :param n_iter: optional number of maximum of iterations
    :param random_state: optional random state to make deterministic

    :returns: X_tsne: the transformed data
    """
    tsne = TSNE(n_components=n_components, perplexity=perplexity, n_iter=n_iter, random_state=random_state)
    X_tsne = tsne.fit_transform(X)

    return X_tsne

def tfidf_on_counts(count_df):
    """
    transforms a dataframe containing counts to an array containing 
    the tf-idf values

    :param cound_df: the dataframe containing the counts

    :returns: array: contains the tf-idf values
    """
    tfidf = TfidfTransformer()
    array = tfidf.fit_transform(count_df)

    return array

def combine_data(vocab, X, cluster_labels):
    """
    combines the vocab, the 2-dimensional word vectors and the cluster 
    labels into one dataframe

    :param vocab: the vocab (from word2vec)
    :param X: the 2-dimensional wordvectors (reduced by tsne)
    :param cluster_labels: the cluster labels (by kmeans)

    :returns: df: a dataframe having the vocab as index, and columns *x*,
       *y* and *cluster*
    """
    df = pd.DataFrame(X, index=vocab, columns=['x', 'y', 'z'])
    df['cluster'] = cluster_labels

    return df

def scale_metric(metric, max_norm=5):
    """
    Takes a list containing the scores of a metric and normalizes them.
    The maximum value to be normalized to is set to 5 per default.

    :param metric: the list of metric values to be normalized
    :param max_norm: the upper bound of values to be normalized to

    :returns: a list of normalized metric values
    """
    max_metric = max(metric)
    return [i*(max_norm/max_metric) for i in metric]

def find_best_k(n_clusters, coh, chs, dbs, ssr):
    """
    normalizes the metrics, scales them against each other and takes the 
    first k with maximum value as best k

    chs and dbs are 'downsampled' as they should only be taken into account 
    if ssr and coh are too similar throghout the k's - for now they are set 
    to zero as using extensive clustering yields enough values to use only 
    ssr and coh

    :param n_clusters: a list containing k's tested as numbers of clusters
    :param coh: the coherence score values
    :param chs: the calinski harabasz score values
    :param dbs: the davies bouldin score values
    :param ssr: the inertia score values

    :returns: the value for the best k that was found
    """
    ssr = scale_metric(ssr)
    coh = scale_metric(coh)
    chs = scale_metric(chs, 0)
    dbs = scale_metric(dbs, 0)

    plot_find_best_k(n_clusters, coh, chs, dbs, ssr)

    res = [a+b-c-d for a,b,c,d in zip(coh, chs, ssr, dbs)]
    best = max(res)
    return n_clusters[res.index(best)]

def add_scores(df, centers, cluster_dict):
    """
    adds a new column called score to the adjectives- and noun-dataframe which 
    contains a value of combined frequency and euclidean distance to the 
    cluster center to measure the relevance (and representativeness) of the word 
    inside its cluster

    :param df_a: the dataframe containing adjectives
    :param df_n: the dataframe containing nouns
    :param cluster_dict: the dict containing the frequencies of each word per cluster

    :returns: 
       tuple containing two items:
          * df_a: the updated dataframe of adjectives
          * df_n: the updated dataframe of nouns
    """
    df['frequency'] = [cluster_dict[df.at[n,'cluster']][n] for n in df.index]
    df['euclidean'] = [distance.euclidean(df.loc[n, df.columns[0:-2]],centers.loc[df['cluster'][n]]) for n in df.index]
    max_e = {n:max(list(df[df['cluster']==n].euclidean)) for n in list(set(df.cluster))}
    df['score'] = [df['frequency'][n] * ((1+max_e[df.at[n,'cluster']]-df['euclidean'][n])/(1+max_e[df.at[n,'cluster']])) for n in df.index]
    df.drop('euclidean', axis=1, inplace=True)

    return df

def find_best_kmeans(n_clusters, data, supervised, random_state=42):
    """
    Helps finding the best number of clusters to perform kmeans on.
    First uses ``MiniBatchKMeans`` to retrieve the ssr and cohesion score 
    for each value in ``n_clusters``, then chooses the best number of clusters 
    and trains a ``KMeans`` model on the data.

    :param n_clusters: a list containing the possible number of clusters to be tested on
    :param data: the data to train the model on
    :param random_state: optional; to make k-means deterministic

    :returns:
       tuple containing two items:
          * kmeans: the trained k-means model
          * best_k: the best value for the number of k-means clusters
    """
    ssr = []
    coh = []
    chs = []
    dbs = []
    n_clusters = [n for n in n_clusters if n < data.shape[0]]

    for k in n_clusters:
        start = time()
        kmeans = MiniBatchKMeans(n_clusters=k, n_init=10, max_iter=900, random_state=random_state).fit(data)
        ssr.append(kmeans.inertia_)
        coh.append(metrics.silhouette_score(data,kmeans.labels_))
        chs.append(metrics.calinski_harabasz_score(data,kmeans.labels_))
        dbs.append(metrics.davies_bouldin_score(data,kmeans.labels_))
        #print(k, ": ", time()-start)

    if supervised:
        plot_find_best_k(n_clusters, coh, chs, dbs, ssr)
        best_k = int(input("Enter best value from ellbow-method: "))
    else:
        best_k = find_best_k(n_clusters, coh, chs, dbs, ssr)
        print("best number of clusters: ", best_k)

    kmeans = KMeans(n_clusters=best_k, n_init=10, max_iter=900, random_state=random_state).fit(data)

    return kmeans, best_k

def kmeans_on_kind(tokens, vocab, n_clusters, start, supervised):
    """
    first reduces the dimensionality of the wordvectors to 2, then finds 
    the best k-means model to use (in terms of number of clusters) and 
    predicts the clusters of the data based on that model; 
    additionally initializes the cluster_dict containing only the names of 
    the clusters mapping to a default value ``0``

    :param tokens: the wordvectors to be reduced in dimensionality
    :param vocab: the vocab (from word2vec)
    :param n_clusters: list containing possible cluster numbers to test
    :param start: the name of the first cluster (will be incremented upon)

    :returns: 
       tuple containing 4 items:
          * df: the dataframe of combined vocab, word-vector and assigned cluster
          * best_k: the best number of clusters found
          * centers: the coordinates of each cluster center
          * cluster_dict: the initialized dict
    """
    X = apply_tsne(tokens, n_components=3)
    kmeans, best_k = find_best_kmeans(n_clusters, X, supervised)
    cluster = kmeans.predict(X)
    cluster = [i+start for i in cluster]
    centers = pd.DataFrame(kmeans.cluster_centers_, index=range(start, start+best_k), columns=['x', 'y', 'z'])

    df = combine_data(vocab, X, cluster)
    cluster_dict = {n:0 for n in range(best_k)} #only initialize -- values will be set later

    return df, best_k, centers, cluster_dict


def kmeans(id, extensive=False, supervised=False):
    """
    The main function to perform k-means on a resource. First gets the 
    prepared data for word2vec which is then performed upon the separated
    data (in terms of word classes). 
    Afterwards performs kmeans on each word class individually to ensure that 
    the clusters of nouns and adjectives are disjoint. 
    Additionally saves the frequency of each word in its cluster to the dataframe 
    of a word class (as a column ``frequency``) and creates a dataframe containing 
    the tfidf scores of each cluster. 
    Finally saves all the computed data to the ``static/clustered/X`` folder where 
    X is the id of the resource clustered.

    :param id: the id of the resource to be clustered
    :param n_clusters: optional, list of possible number of cluster for kmeans to be 
       tested on
    :param supervised: optional, whether to choose the optimal number of clusters 
       automatically (``False``) or by user (``True``)

    :returns: None
    """
    # set min counts for word2vec based on source type (articles much smaller --> smaller min_count)
    if id == 'articles':
        min_count = 2
    elif id == 'all':
        min_count = 15
    else: 
        min_count = 10

    # get clusters using word2vec and kmeans
    data, pos_dict = get_clean_data(id)
    print("corpus done...")
    # if no randomness is desired, seed and only one worker is required
    vocab_n, tokens_n, vocab_a, tokens_a = separate_word2vec(data, pos_dict, min_count=min_count)
    #print(len(vocab_a), " , ", len(vocab_n))
    print("word2vec done...")
    
    # set n_clusters to provide a fast and an extensive clustering mode 
    if extensive:
        # quick hack: only use len of nouns as there are typically more nouns than adjectives 
        # in a resource - while clustering the list is reduced to the len of nouns/adjs
        if len(vocab_n) > 600:
            n_clusters = list(range(2, 600, 3)) + list(range(600, len(vocab_n), (len(vocab_n)-600)//100))
        elif len(vocab_n) > 300:
            n_clusters=list(range(2, len(vocab_n), len(vocab_n)//200))
        else:
            n_clusters = list(range(2, len(vocab_n)))
    else:
        n_clusters=[2,5,8,10,13,16,20,25,30,40,80,100,130,160,200,230,260,300,350,400,450,500,1000,1500,2000,2500]

    df_a, best_k_a, centers_a, cluster_dict_a = kmeans_on_kind(tokens_a, vocab_a, n_clusters, 0, supervised)
    df_n, best_k_n, centers_n, cluster_dict_n = kmeans_on_kind(tokens_n, vocab_n, n_clusters, best_k_a, supervised)
    cluster_dict = {**cluster_dict_a, **cluster_dict_n}
    n_clusters = best_k_a + best_k_n
    
    # get tfidf values to estimate most relevant clusters
    data_tfidf, cluster_dict = prepare_counts(id, df_a, df_n, cluster_dict, n_clusters)
    df_a = add_scores(df_a, centers_a, cluster_dict)
    df_n = add_scores(df_n, centers_n, cluster_dict)
    X_tfidf = tfidf_on_counts(data_tfidf).toarray()
    df_tfidf = pd.DataFrame(data=X_tfidf, index=list(range(n_clusters))[best_k_a:], columns=list(range(n_clusters))[:best_k_a])

    df_a.to_csv(str(Path(__file__).parent) + "/static/clustered/kmeans/{}/df_a.csv".format(id))
    df_n.to_csv(str(Path(__file__).parent) + "/static/clustered/kmeans/{}/df_n.csv".format(id))
    df_tfidf.to_csv(str(Path(__file__).parent) + "/static/clustered/kmeans/{}/df_tfidf.csv".format(id))

    cluster_dict = {'kind':id, 'best_k_a': best_k_a, 'best_k_n': best_k_n}
    with open(str(Path(__file__).parent) + "/static/clustered/kmeans/{}/dict.json".format(id), 'w') as f:
        json.dump(cluster_dict, f)