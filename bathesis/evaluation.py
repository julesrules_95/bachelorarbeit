import json
import numpy
import pandas
from pathlib import Path
from math import comb

from  bathesis.cleaning import prepare_df_cleaning

def remove_missing_words(df_classic, df_bidi, df_counts):
    """
    helper function to remove words only occurring in one df but not 
    the other one

    :param df_classic: the results of 'classic' clustering e.g. kmeans
    :param df_bidi: the results of bidirectional clustering
    :param df_counts: the dataframe containing the cleaned counts of the resource

    :returns: df_counts: the counts of only the mutual words
    """
    words = [w for w in df_bidi.index if w in df_classic.index]
    df_counts = df_counts.loc[words]

    return df_counts

def get_clean_results(id, method='kmeans', size=50):
    """
    this function will take a resource kind as argument and retrieve 
    the results of both the bidirectional and classical clustering; from 
    that point on it will only leave those datapoints occurring in both 
    results and choose the 50 most frequent words - nouns and adjectives - 
    to be evaluated against a gold standard

    :param id: the id of the resource to get gold-words for
    :param method: the method used for classic clustering
    :param size: the number of words to be retrieved per word-type (x most common words)

    :returns:
       tuple containing two items:
          * adj: the adjectives to be used for gold standard
          * nouns: the nouns to be used for gold standard
    """
    # load 5 sources: both results and the cleaned counts 
    df_a_classic = pandas.read_csv(str(Path(__file__).parent) + "/static/clustered/{}/{}/df_a.csv".format(method,id), index_col=0)
    df_n_classic = pandas.read_csv(str(Path(__file__).parent) + "/static/clustered/{}/{}/df_n.csv".format(method,id), index_col=0)
    df_a_bidi = pandas.read_csv(str(Path(__file__).parent) + "/bidirectional/data/result/{}/adj.csv".format(id), sep=';', index_col=1)
    df_n_bidi = pandas.read_csv(str(Path(__file__).parent) + "/bidirectional/data/result/{}/noun.csv".format(id), sep=';', index_col=1)
    df_counts = pandas.read_csv(str(Path(__file__).parent) + "/static/cleaned/{}-counts-cleaned.csv".format(id), index_col=0)

    # remove column 'word' as the index already is the word
    df_a_bidi.drop('word', axis=1, inplace=True)
    df_n_bidi.drop('word', axis=1, inplace=True)

    # get dfs of only counts
    indices, cols, sum_all, df_nouns, df_adj = prepare_df_cleaning(df_counts)

    # remove all words only occurring in one df but not the other
    df_adj = remove_missing_words(df_a_classic, df_a_bidi, df_adj)
    print(df_a_classic.shape, " , ", df_a_bidi.shape, df_adj.shape)
    df_nouns = remove_missing_words(df_n_classic, df_n_bidi, df_nouns)
    print(df_n_classic.shape, " , ", df_n_bidi.shape, df_nouns.shape)

    # sort counts descending and keep only the 50 most frequent words from each source
    df_adj = df_adj.sort_values(by='count', ascending=False).iloc[0:size]
    adj = df_adj.index
    df_nouns = df_nouns.sort_values(by='count', ascending=False).iloc[0:size]
    nouns = df_nouns.index

    return adj, nouns

def get_words_for_gold():
    """
    Function that puts together the 50 most frequent words of each 
    resource to get all the words that have to be clustered for the 
    gold standard. Saves them as dataframes in format adj*adj or 
    noun*noun having only zeros as values to be able to denote 
    related words.
    """
    adj = set()
    nouns = set()

    for kind in ['articles', 'recipes', 'comments', 'all']:
        l_adj, l_nouns = get_clean_results(kind, size=50)
        adj.update(l_adj)
        nouns.update(l_nouns)

    adj = pandas.DataFrame(numpy.zeros((len(adj),len(adj))), index=list(adj), columns=list(adj))
    nouns = pandas.DataFrame(numpy.zeros((len(nouns),len(nouns))), index=list(nouns), columns=list(nouns))

    adj.to_csv(str(Path(__file__).parent) + "/static/evaluation/gold/adj.csv")
    nouns.to_csv(str(Path(__file__).parent) + "/static/evaluation/gold/nouns.csv") 


def get_gold_standard(kind):
    """
    Loads the results of the gold-standard-surveys into a df and creates a 
    list of all the matches, which is then saved as ``X-gold.txt`` where X 
    is the word-type clustered.
    """
    df = pandas.read_csv(str(Path(__file__).parent) + "/static/evaluation/gold/{}-1.csv".format(kind), sep=';', index_col=0)
    #print(df.shape)
    pairs = []

    # remove empty rows & cols to speed up processing
    #first remove empty rows
    df = df[(df.T != 0).any()]
    # now remove emtpy cols
    df = df.T[(df != 0).any()].T

    cols = list(df.columns)
    #iterate over rows and add index+col to every list where not zero
    for col in cols:
        df_col = df[df[col]!=0]
        for i in df_col.index:
            pairs.append([i,col])

    with open(str(Path(__file__).parent) + "/static/evaluation/gold/{}-gold.txt".format(kind), "w") as f:
        f.write(json.dumps(pairs))


def build_gold_list():
    """
    Wrapper function to build gold list for each word type.
    """
    for kind in ['adj', 'nouns']:
        get_gold_standard(kind)

def eval_on_kind(id, kind, method, words_dict, gold):
    """
    Function that runs the evaluation using rand index for a 
    specific set of results.

    It does so by first calculating the number of possible pairs of 
    2 matching words by permuting all clusters (of the result) having 
    2 or more words in them (saved as ``fp``). Then it calculates the 
    total number of possible matches (``comb(len(words),2)``) and removes 
    ``fp`` which is stored as ``tn``.

    Now the function iterates over the list of pairs from the gold 
    standard and if the pair is also a pair in the result-df it increses 
    ``tp`` by 1 while decreasing ``fp`` by 1. If the pair isn't found 
    ``fn`` is increased and ``tn`` decreased.

    Lastly the actual rand index is calculated and the total numbers as well 
    as the index value are stored in a json-file. 

    :param id: the id of the resource e.g. ``articles`` 
    :param kind: the kind of word evaluated e.g. ``adj``
    :param method: the clustering method used
    :param words: the words used for the gold-standard
    :param gold: the pairs of related words from the gold standard

    :returns: results: the dict containing each evaluation value
    """
    df_clustered = pandas.read_csv(str(Path(__file__).parent) + "/static/clustered/{}/{}/df_{}.csv".format(method, id, kind), index_col=0)

    tp = 0
    fp = 0
    tn = 0
    fn = 0

    # set rows and column name 
    words = words_dict['{}_{}'.format(id,kind)]
    if method == 'bidirectional':
        col = 'class'
    else:
        col = 'cluster'
    #reduce size to only words in gold-standard
    df_clustered = df_clustered.loc[words, [col]]
    gold = [p for p in gold if p[0] in words and p[1] in words]

    # find overall positives and negatives from clustered df
    for i in set(sorted(list(df_clustered[col]))): 
        if df_clustered[df_clustered[col]==i].shape[0] >= 2:
            fp += comb(df_clustered[df_clustered[col]==i].shape[0],2)
    tn = comb(len(words), 2) - fp
    # now find tp and fn by iterating over gold-list
    for word1, word2 in gold:
        if df_clustered.at[word1, col] == df_clustered.at[word2, col]:
            tp += 1
            fp -= 1
        else:
            fn += 1
            tn -= 1

    ri = (tp+tn)/(tp+fp+tn+fn)
    p = tp/(tp+fp)
    r = tp/(tp+fn)

    # now save the results for vis later-on
    results = {'tp': tp, 'tn':tn, 'fp':fp, 'fn':fn, 'ri':ri, 'p':p, 'r':r}

    with open(str(Path(__file__).parent) + "/static/evaluation/{}/{}-{}-eval.json".format(id, method, kind), "w") as f:
        json.dump(results, f)

    return results

def run_eval():
    """
    Function to run pairwise rand index using the results of clustering and the gold-list.
    
    First loads the gold-csv's and takes their indexes as the words to be compared which, 
    in a second step, are used to create a specific words list for each resource kind - 
    such that the words do not differ between the different algorithms used to yield 
    valuable results. These lists are stored in a dict to be easily accessible later on.

    Then runs the actual evaluation for each method and resource.
    """
    #first use gold-csv to get the words to be compared
    df_adj = pandas.read_csv(str(Path(__file__).parent) + "/static/evaluation/gold/{}-1.csv".format('adj'), sep=';', index_col=0)
    df_n = pandas.read_csv(str(Path(__file__).parent) + "/static/evaluation/gold/{}-1.csv".format('nouns'), sep=';', index_col=0)

    eval_adj = df_adj.index
    eval_n = df_n.index

    words_dict = {}

    with open(str(Path(__file__).parent) + "/static/evaluation/gold/{}-gold.txt".format('adj')) as f:
        gold_adj = json.loads(f.read())
    with open(str(Path(__file__).parent) + "/static/evaluation/gold/{}-gold.txt".format('nouns')) as f:
        gold_n = json.loads(f.read())

    # prepare word list as it has to be the same for each resource to 
    # produce valuable results
    for id in ['articles', 'comments', 'recipes', 'all']:
        tmp_a = eval_adj.copy()
        tmp_n = eval_n.copy()
        for method in ['aggl', 'bidirectional', 'kmeans']:
            df_a = pandas.read_csv(str(Path(__file__).parent) + "/static/clustered/{}/{}/df_{}.csv".format(method, id, 'a'), index_col=0)
            tmp_a = [w for w in tmp_a if w in df_a.index]
            df_n = pandas.read_csv(str(Path(__file__).parent) + "/static/clustered/{}/{}/df_{}.csv".format(method, id, 'n'), index_col=0)
            tmp_n = [w for w in tmp_n if w in df_n.index]
        #print(id, "adjectives:", len(tmp_a), "nouns:", len(tmp_n))
        words_dict['{}_a'.format(id)]=tmp_a
        words_dict['{}_n'.format(id)]=tmp_n

    # now run actual evaluation algorithm on each resource
    for method in ['aggl', 'bidirectional', 'kmeans']:
        for id in ['articles', 'comments', 'recipes', 'all']:
            results_a = eval_on_kind(id, 'a', method, words_dict, gold_adj)
            results_n = eval_on_kind(id, 'n', method, words_dict, gold_n)
            results = {'tp': results_a['tp']+results_n['tp'], 'tn':results_a['tn']+results_n['tn'], 
                'fp':results_a['fp']+results_n['fp'], 'fn':results_a['fn']+results_n['fn']}
            results['ri'] = (results['tp']+results['tn'])/(results['tp']+results['tn']+results['fp']+results['fn'])
            results['p'] = results['tp']/(results['tp']+results['fp'])
            results['r'] = results['tp']/(results['tp']+results['fn'])

            with open(str(Path(__file__).parent) + "/static/evaluation/{}/{}-eval.json".format(id, method), "w") as f:
                json.dump(results, f)

    print("done evaluating")