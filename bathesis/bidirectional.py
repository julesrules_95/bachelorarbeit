import re
import traceback
import pandas as pd
import rpy2.robjects as robjects

from pathlib import Path

def apply_bidirectional(corpus):
    """
    Function to call the ``bidirectional`` method from the bidirectional r-algorithm.
    After running the algorithm the results are postprocessed in terms of normalizing the names 
    of the clusters and storing information about potential subclusters as well as adding some 
    (arbitrary) coordinates for visualizations and then saved to the ``static/clustered/bidirectional`` 
    folder.

    :param corpus: the kind of corpus to use being ``articles``, ``comments``, 
        ``recipes`` or ``all``

    :returns: None
    """
    r = robjects.r
    r.source('~/Desktop/Studium/Bachelorarbeit/bachelorarbeit/bathesis/bidirectional/bidi-clustering-adjectives-R/adj_noun_wiebke.R')
    r.bidirectional(corpus)
    #r.source('~/Desktop/Studium/Bachelorarbeit/bachelorarbeit/bathesis/bidirectional/bidi-clustering-adjectives-R/bidirectional_testing.R')
    prepare_df_vis(corpus)
    return


def prepare_df_vis(res):
    """
    The wrapper for postprocessing the bidirectional results.
    Results are first loaded into a dataframe which are used to rename the classes 
    to ascending integer values and to add coordinates to each word. Additionally 
    the subclasses of each class are also stored in a seperate dataframe for visualization 
    later on.

    :param res: the id of the resource to be preprocessed

    :returns: None
    """
    try:
        df_adj = pd.read_csv(str(Path(__file__).parent) + "/bidirectional/data/result/{}/adj.csv".format(res), sep=';')
        df_adj.set_index('word', inplace=True)
        df_n = pd.read_csv(str(Path(__file__).parent) + "/bidirectional/data/result/{}/noun.csv".format(res), sep=';')
        df_n.set_index('word', inplace=True)
    except Exception as e:
        traceback.print_exc()
        return

    df_adj.loc[df_adj['class'] == 'selfclass', 'class'] = 0
    df_adj = df_adj.astype(int)
    #print(df_adj)
    sub_dict_adj = add_subclasses(res, "adj")
    df_adj, df_subclasses_adj = rename_classes(df_adj, sub_dict_adj, 1)
    df_adj, df_subclasses_adj = add_coordinates(df_adj, df_subclasses_adj)
    df_adj.to_csv(str(Path(__file__).parent) + "/static/clustered/bidirectional/{}/df_a.csv".format(res))
    df_subclasses_adj.to_csv(str(Path(__file__).parent) + "/static/clustered/bidirectional/{}/df_subclasses_adj.csv".format(res))

    start_n_classes = int(max(df_adj['class'].tolist())+1)

    df_n.loc[df_n['class'] == 'selfclass', 'class'] = 0
    df_n = df_n.astype(int)
    #print(df_n)
    sub_dict_n = add_subclasses(res, "noun")
    df_n, df_subclasses_n = rename_classes(df_n, sub_dict_n, start_n_classes)
    df_n, df_subclasses_n = add_coordinates(df_n, df_subclasses_n)
    df_n.to_csv(str(Path(__file__).parent) + "/static/clustered/bidirectional/{}/df_n.csv".format(res))
    df_subclasses_n.to_csv(str(Path(__file__).parent) + "/static/clustered/bidirectional/{}/df_subclasses_n.csv".format(res))

    return


def rename_classes(df, sub_dict, start):
    """
    Takes a the bidirectional results and information about their 
    subclasses. Then normalizes the class names to ascending numbers 
    starting from a given integer.

    :param df: the dataframe of bidirectional results for a resource 
    :param sub_dict: a dict of subclasses found in that resource
    :param start: the integer for the classes to start with

    :returns: 
       tuple containing two items:
          * df: the dataframe of results with normalized class values
          * df_subclasses: a dataframe of normalized subclasses
    """
    existing_names = sorted(list(set(df['class'].tolist())))
    existing_names.remove(0)
    print(df.shape[0])
    print(len(existing_names))
    zeros = df[df['class']==0]
    print(len(zeros))
    new_classes = len(existing_names)-1
    needed_names = [i for i in list(range(start,start+len(existing_names)+len(zeros))) if i not in existing_names]
    removing_names = [i for i in existing_names if i not in list(range(start,start+len(existing_names)+len(zeros)))]
    removing_names.append(0)
    #print(existing_names, "\n", needed_names, len(needed_names), "\n", removing_names, len(removing_names))

    #print("before: \n", df, "\n", sub_dict)
    for n in needed_names:
        if removing_names[0] != 0:
            df.loc[df['class'] == removing_names[0], 'class'] = n
        else: 
            #change each class-0-element (single element cluster) individually
            tmp = df.loc[df['class'] == removing_names[0], 'class']
            tmp.iloc[0] = n
        if removing_names[0] in sub_dict.keys():
            sub_dict[n] = sub_dict.pop(removing_names[0])
        if removing_names[0] in sub_dict.values():
            for key, value in sub_dict.items():
                # classes cannot be subclasses of themselves
                if value == removing_names[0]:
                    sub_dict[key] = n
        if removing_names[0] == 0:
            # no class-0-elements left?
            if df.loc[df['class'] == removing_names[0], 'class'].shape[0] == 0:
                removing_names.remove(removing_names[0])
        else:
            removing_names.remove(removing_names[0])
    #print("after: \n", df, "\n", sub_dict)


    for i,n in zip(df.loc[df['class'] == 0].index, list(range(new_classes, new_classes+len(df.loc[df['class'] == 0])))):
        df.at[i,'class'] = n
        # only changing selfclass of which none are superclassed

    # create dataframe of subclasses 
    df_subclasses = pd.DataFrame.from_dict(sub_dict, orient='index', columns=['class'])
    
    return df, df_subclasses

def add_subclasses(id, kind):
    """
    Adds all classes below a class in hierarchy as subclasses for visualization.
    Does so by using a regex to look up potential subclasses in the ``history-{}.dat.final``.

    :param id: the id of the resource which was clustered
    :param kind: the word type that was clustered 

    :returns: sub_dict: dictionary mapping from found subclusters to their supercluster
    """
    try:
        with open(str(Path(__file__).parent) + "/bidirectional/data/result/{}/history-{}.dat.final".format(id, kind)) as f:
            results = f.readlines()
    except Exception as e:
        traceback.print_exc()
        return

    sub_dict = {}

    class_regex = re.compile(r"^(\d+)(?:\[.*?\]<)((?:\s?\d+)*)(?:>.*?)$")

    for result in results:
        m = class_regex.match(result)
        if m.group(2):
            #print("Group2: ", m.group(2))
            subclasses = m.group(2).split()
            for s in subclasses:
                sub_dict[int(s)] = int(m.group(1))

    return sub_dict

def add_coordinates(df, df_subclasses):
    """
    Adds x and y values for each data point to visualize later on. 
    Coordinates are set arbitrarily at y=1 and ascending from x=1.
    The coordinates do not contain any additional information.

    :param df: dataframe of results from bidirectional clustering
    :param df_subclasses: dataframe of found subclusters

    :returns:
       tuple containing two items:
          * df: df with added coordinates
          * df_subclasses: df_subclasses with added coordinates
    """
    # prepare for adding coordinates
    df['x'] = [int(0)]*len(df.index)
    df['y'] = [int(1)]*len(df.index)
    df_subclasses['x'] = [int(0)]*len(df_subclasses.index)
    df_subclasses['y'] = [int(1)]*len(df_subclasses.index)

    # get sets of existing classes
    df_classes = set(df['class'])
    df_subclasses_classes = set(df['class'])

    for c in df_classes:
        x = 1
        for word in df[df['class']==c].index:
            df.at[word, 'x'] = x
            x+=1
        if c in df_subclasses_classes:
            for word in df_subclasses[df_subclasses['class']==c].index:
                df_subclasses.at[word, 'x'] = x
                x+=1

    return df, df_subclasses