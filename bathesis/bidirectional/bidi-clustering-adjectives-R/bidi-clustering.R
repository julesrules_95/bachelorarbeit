#

# Bidirectional clustering
# @dnfirst The data matrix, 3 cols: N, A, frequency (integer)
# @binary boolean: Transform any frequencies into binary values?
# @binary.merge.method string: and, or, majority
# @distance.method String: Which evaluation method is chosen by the C++ distance calculation?
#   Possible values:
#   *  jaccard :: Jaccard distance
#   *  jaccardconfidence :: J. distance with a confidence range.
bidi.cluster = function(dnfirst, binary, binary.merge.method, p, q, n.non.sparse, distance.method, verbose=FALSE)
{
  #####################################
  # nl: set of all nouns
  nl = sort(unique(dnfirst$noun))
  # al: set of all adjectives
  al = sort(unique(dnfirst$adj))
  
  print(sprintf("number of adjectives (types): %d", length(al)), quote=FALSE)
  print(sprintf("number of nouns (types): %d", length(nl)), quote=FALSE)
  print(sprintf("number of sequences (tokens): %d", sum(dnfirst$freq)), quote=FALSE)
  print(sprintf("expected sparseness: cells per tokens %.3f", length(al)*length(nl)/sum(dnfirst$freq)), quote=FALSE)
  
  
  # Initialization: myfreq table noun x adj with freq values
  myfreq = c()
  if(binary==FALSE){
    myfreq = xtabs(I(freq)~noun+adj, data=dnfirst, sparse=TRUE)
  }else{
    # alternativ ohne Frequenzen
    myfreq = xtabs(~noun+adj, data=dnfirst, sparse=TRUE)
  }
  
  # myfreq_bu: backup of the original frequency table, attribute set and noun set
  myfreq_bu = myfreq
  nl_bu = nl
  al_bu = al
  
  ################################################################
  
  # filter out all nouns which only occur not more than nc adjectives and all 
  # adjectives which only occur with maximal nc nouns
  
  #initialising 
  nl1 <- nrow(myfreq)
  myfreq <- remove_sparse_rows(myfreq, n.non.sparse)
  nl2 <- nrow(myfreq) 
  
  al1 <- ncol(myfreq)
  myfreq <- remove_sparse_cols(myfreq, n.non.sparse)
  al2 <- ncol(myfreq) 
  
  #loop until no row or column with at least nc non-zeros occur anymore
  # k?nnte man noch vereinfachen mit t(myfreq), das Reihen und Spalten vertauscht
  
  while((nl1!=nl2)|(al1!=al2)){
    nl1 <- nrow(myfreq)
    myfreq <- remove_sparse_rows(myfreq, n.non.sparse)
    nl2 <- nrow(myfreq) 
    
    al1 <- ncol(myfreq)
    myfreq <- remove_sparse_cols(myfreq, n.non.sparse)
    al2 <- ncol(myfreq)
  }
  
  # backup of the reduced frequency table
  myfreq_bu_red <- myfreq
  
  # reduced adjective and noun set
  
  nl <- rownames(myfreq)
  al <- colnames(myfreq)
  # normalizing 
  # row sum adds up to 1
  # vielleicht nicht ideal, da hierbei adj-noun Kombinationen ?berbewertet werden, in denen das Nomen selten ist.
  if(binary==FALSE){
    # TODO Is the condition correct? Do we need to normalize in binary settings? Or a separate parameter?
    # myfreq = normalize(myfreq)# original: rowwise normalization
    # tf-idf
    #browser()
    N = nrow(myfreq) # number of 'documents'
    df = apply(myfreq, 2, function(x)return(length(which(x>0)))) # how often does each term = A occur in each document?
    idf = log(N/df)
    for(i in 1:nrow(myfreq)){
      myfreq[i,] = myfreq[i,] * idf
    }
  }else{
    # force binarization
    myfreq[which(myfreq>0)] = 1
  }
  
  
  ###############################################################
  # partioning around medoids
  # # dauert viel zu lange, um es öfter zu machen, daher eigene Implementierung (sieh unten)
  
  
  #####################################################################
  #####################################################################
  # Gro?e Schleife
  # Idee: nach jeder Runde wird mit t(myfreq) die matrix gespiegelt und Nomen und Adjektive tauschen die Rollen
  
  ########################################################
  # brute force clustering 
  # (kein Algorithmus, der nach optimaler Verteilung sucht, sondern knallhart nach sortierter ?hnlichkeitsma?liste)
  
  # current number of nouns 
  cur_n <- nrow(myfreq)
  # current number of adjectives
  cur_a <- ncol(myfreq)
  
  # total number of nouns
  tot_n <- nrow(myfreq)
  # total number of adjectives
  tot_a <- ncol(myfreq)
  
  # Initializing class tables
  # noun table
  nl<- rownames(myfreq)
  nounClass<-data.frame(word=nl,class=nl,stringsAsFactors=FALSE)
  # adj table
  al<- colnames(myfreq)
  adjClass<-data.frame(word=al,class=al,stringsAsFactors=FALSE)
  
  
  # Initializing while-loop (first nouns are clustered by adj)
  wordClass <- nounClass
  wl <- nl
  word_class <- "noun"
  
  # counter
  ct <- 1
  
  print(sprintf("%d Adjektive", as.integer(cur_a)), quote=FALSE)
  print(sprintf("%d Nomina", as.integer(cur_n)), quote=FALSE)
  
  logtreshold<- c()
  nounRecord <- t(nounClass) 
  rownames(nounRecord)<-c()
  adjRecord <-  t(adjClass) 
  rownames(adjRecord)<-c()
  
  # OH perhaps, we can estimate the value of 'b', so we don't need to recalculate the full distance matrix in every epoch?
  m.dims = c()# dimension of the distance matrix
  bs = c() 
  
  ################################################
  ################################################
  ################################################
  ################################################
  ################################################
  ################################################
  ################################################
  ################################################
  History = list() # key = cxxx(number of a group) => value: (list: items in this group, noun?)
  Edgelist = data.frame() # 1st column: parent, 2nd column: child
  step = 1
  while(cur_n > q*tot_n & cur_a > q*tot_a) # Start der gro?en while-loop
  # q*tot_n oder q*tot_a ist minimale anzahl cluster
  {
    print(sprintf(" current matrix size: %d x %d", nrow(myfreq), ncol(myfreq)), quote=FALSE)
    #distance matrix
    # OH Generell: Wir bekommen hier in jedem Fall Schwierigkeiten bei real-world Problemen.
    # Idee: Man könnte die Schwellenwerte 'b' an einem Testset vorberechnen und dann eine glatte Funktion Schwellenwert ~ nrow(matrix) erstellen.
    # Dann geht man zeilenweise durch die Matrix und zeichnet nur die Datensätze auf, die unter dem approximierten Schwellenwert liegen.
    # D.h. man baut list1 und list2 direkt aus dieser Iteration. Mag vielleicht etwas länger dauern, wird aber nicht den Computer sprengen.
    # TODO andere Distanzfunktion: Nur zählen, wenn beide 1 oder wenn einer 1 und einer 0; Jaccard?
    b = 0
    list1 = c()
    list2 = c()
    if(FALSE)
    {
      
    }else{
      # much faster: Use C++
      # Sample command line
      # -nearest -pathin "C:\MyFiles\Uni\Düsseldorf\C10\R\wiebke-iter-clustering\data\test.triplets" 
      #   -pathout "C:\MyFiles\Uni\Düsseldorf\C10\R\wiebke-iter-clustering\data\result.triplets" 
      #   -similarity jaccard 
      #   -minintersection 4 
      #   -nkeep 500
      #browser()
      threshold = ifelse(binary==TRUE, 0, 1e-04)
      write.triplets(myfreq, INPATH, threshold)
      #browser()
      unlink(OUTPATH) # remove results from a previous loop
      p.noun.jaccard = 0.4 #0.5
      p.adj.jaccard =  0.2
      # @PARAMETER @threshold: Pairwise similarity must be higher than this value for acceptance
      # In general: small values = larger, but less pure clusters
      threshold = 0
      if(binary==TRUE){
        threshold = ifelse(word_class=="noun", p.noun.jaccard, p.adj.jaccard) # this is for JACCARD!!! From 'threshold.R'
      }else{
        if(distance.method=="euclidean"){
          threshold = 2000 # TODO This is a hack!
        }
      }
      # @PARAMETER
      # Important parameter: Minimal number of common terms for accepting a pair.
      # Can be calculated with 'threshold.R'.
      minintersection = ifelse(word_class=="noun", 8,5) #10, 5)
      # @PARAMETER
      # TODO Motivate this parameter!!!
      nkeep = as.integer(max(0.1 * nrow(myfreq), 1))  # o.05
      cmdline = sprintf("%s -nearest -pathin %s -pathout %s -similarity %s -threshold %.5f -minintersection %d -nkeep %d",
        EXEPATH, INPATH, OUTPATH,
        distance.method,
        threshold,
        minintersection,
        nkeep
        )
      retval = system(cmdline)
      if(retval!=0){
        print("Error in C++! break!!", quote = FALSE)
        break
      }
      input = try(read.delim(OUTPATH, sep=" ", header=FALSE,  stringsAsFactors=FALSE, row.names=NULL))
      if(inherits(input, "try-error")){
        print("No new candidates from C++ ... break", quote = FALSE)
        break
      }
      list1 = as.integer(input[,1]+1) # increase the indices from c++!
      list2 = as.integer(input[,2]+1)
      b = as.double(input[nrow(input),3])
    }
    if(length(list1)==0 | length(list2)==0){
      print(" --- No more results from similarity detection, break! ---", quote=FALSE)
      break
    }
    logtreshold = c(logtreshold,b)
    bs = c(bs, b); m.dims = c(m.dims, nrow(myfreq))
    plot(bs~m.dims, t="l")
    
    ##############################################################################
    
    # jetzt wird es interessant, ich muss die Elemente, die zu einer Klasse geh?ren aufsammeln
    # durch die Dopplung erh?lt man einen Pseudodigraphen, das macht die Berechnung der Äquivalenzklassen leichter.
    l1<- c(list1,list2) # rows, cols
    l2<- c(list2,list1) # cols, rows.
    
    nrow.old = nrow(myfreq)
    ncol.old = ncol(myfreq)
    myfreqnew = myfreq[0,] # leere Frequenzmatrix
    
    # berechnet transivite Hülle (bzw. Äquivalenzklassen)
    # ll1 enth?lt Liste aller Indizes in der Wortliste, die am Ende nicht wegfallen
    # OH ll1 = indices aller records, die nicht in l1 enthalten sind.
    ll1 = which(!is.element(1:nrow(myfreq),l1)) # alt: ll1 = which(!(c(1:nrow(myfreq)) %in% l1))
    
    while(length(l2)>0)
    {
      ec = l2[1] # first elm from l2, may be a row or a column (?)
      ec1 = union(ec,l1[which((l2==l2[1]))]) # take the corresponding other dimension from l1; ec1 has one more elm than ec
      while(length(ec) != length(ec1)){
        ec = ec1 # swap
        ec1 = union(ec,l1[which(is.element(l2,ec))]) # union set, duplicates are removed.
      }
      # no more elements could be added to ec1
      label = sprintf("%s%d", CLASS_PREFIX, ct) # for easier grepping
      items = wl[ec1]
      if(verbose){
        print(sprintf("range ec1: %d - %d: %s", min(ec1), max(ec1), paste(items, collapse=" ")), quote=FALSE)
      }    
      w.groups = grep(CLASS_PREFIX, items) # which of these denote groups?
      atoms = items
      if(length(w.groups)>0){
        atoms = items[-w.groups]
      }
      groups= c()
      if(length(w.groups)>0){
        groups = items[w.groups]
      }
      
      wordClass$class[which(is.element(wordClass$class,rownames(myfreq)[ec1]))] = label
      for(item in items){
        Edgelist = rbind(Edgelist,data.frame(cbind(parent=label, child=item), stringsAsFactors = FALSE))
      }
      # reduce l1 and l2 to the yet uncovered cases.
      vh = !is.element(l1,ec1)
      l1 = l1[vh]
      l2 = l2[vh]
      ct = ct+1
      #browser()
      # myfreqnew sammelt neue Zeilen = creates a new center from the involved vectors.
      # This is governed by @binary.merge.method!
      new.row = rep(0, ncol(myfreq))
      cs = colSums(myfreq[ec1,])
      w.features = which(cs>0)
      feat.table = data.frame()
      if(length(w.features)>0){
        feat.table = data.frame(cbind(word=colnames(myfreq)[w.features],freq=cs[w.features]), stringsAsFactors = FALSE)
        feat.table$freq = as.integer(feat.table$freq)
        feat.table = feat.table[order(feat.table$freq,decreasing = TRUE),]
        rownames(feat.table) = NULL
      }
      History[[label]] = list('atoms' = atoms, 
        'groups' = groups, 
        'word.class' = word_class,
        'features' = feat.table,
        'handled' = FALSE, 
        'label' = label)
      if(binary==TRUE){
        w = c()
        if(binary.merge.method=="and"){
          w = which(cs==length(ec1))
        }else if(binary.merge.method=="or"){
          w = which(cs>0)
        }else if(binary.merge.method=="majority"){
          # @PARAMETER /3!! majority = 1/3 of all items have the same value; or 1 for two items.
          t = max(1, length(ec1)/3)
          w = which(cs>=t) # TODO other weighting?
        }else{
          print("Unsupported binarization method!", quote=FALSE)
          browser() # break
        }
        if(length(w)==0){
          # TODO Diese Gruppe nicht bilden! Wenn keine weiteren Gruppen gefunden, das Clustern abbrechen.
          print("WARNING: new row has only zeros, keeping the old ungrouped data!")
          new.row = myfreq[ec1,] # take the original data.
        }else{
          new.row[w] = 1
        }
      }else{
        s = sum(cs)
        if(s==0){
          print("WARNING: new row has only zeros, keeping the old ungrouped data!")
          new.row = myfreq[ec1,]
        }else{
          new.row = colMeans(myfreq[ec1,]) #cs/s#normalize(cs)
        }
      }
      myfreqnew = rbind2(myfreqnew,new.row) # add a new row (= new class) to mfreqnew
      rownames(myfreqnew)[nrow(myfreqnew)] = label
    }
    # Aktualisierung von myfreq
    # erst Normalisieren von myfreqnew
    # myfreqnew <- normalize(myfreqnew) # done above
    # dann verkleben: An die nicht-behandelten Records von myfreq (::myfreq[ll1,]) werden die neuen geclusterten Records (::myfreqnew) angehängt.
    myfreq <- rbind2(myfreq[ll1,],myfreqnew)
    # Abbruch: Wenn sich die Dimension nicht geändert hat, ist nix passiert.
    if(nrow(myfreq)==nrow.old){
      print(" Keine Veränderung in myfreq --> break", quote = FALSE)
      break
    }
    # transformiere alles f?r n?chsten Durchlauf: d.h. Vertauschen von noun/adj.
    myfreq = t(myfreq)
    # normalizing 
    # Ist eigentlich sichergestellt, dass nie durch 0 geteilt werden kann? OH: Ja, habe die Fun geändert.
    if(binary==FALSE){
      myfreq = normalize(myfreq)
    }else{
      # sanity: make sure that only 0 and 1 are in the matrix.
      w = which(myfreq!=0 & myfreq!=1)
      if(length(w)>0){
        browser()
      }
    }
    if(word_class =="noun"){
      nl = colnames(myfreq)
      wl = al
      nounClass = wordClass
      nounRecord = rbind(nounRecord,nounClass[,2])
      wordClass = adjClass
      word_class = "adj"
    }else{
      al = colnames(myfreq)
      wl = nl
      adjClass <- wordClass
      adjRecord <- rbind(adjRecord,adjClass[,2])
      wordClass <- nounClass
      word_class <- "noun"
    }
    
    
    cur_a <- length(al)
    cur_n <- length(nl)
    
    print(sprintf("Durchlauf '%s' geschafft ... %d verbleibende Adjektive, %d verbleibende Nomina", word_class, cur_a, cur_n), quote=FALSE)
    step = step+1
  } # Ende der großen while-loop Schleife
  
  
  
  
  # have a look at the thresholds
  # Warum unterscheiden die sich? 'odd' verstehe ich (cluster nouns with adj), aber nicht 'even'
  dfb = data.frame(cbind(b=bs, rows=m.dims), stringsAsFactors = FALSE)
  o = par(mfrow=c(1,2))
  i = seq(1,nrow(dfb), by=2)
  plot(dfb[i,]$b~dfb[i,]$rows,t="l", main="odd")
  i = seq(2,nrow(dfb), by=2)
  plot(dfb[i,]$b~dfb[i,]$rows,t="l", main="even")
  par(o)
  
  nounClass = nounClass[order(nounClass$class),]
  nounRecord = data.table(nounRecord)
  adjClass = adjClass[order(adjClass$class),]
  adjRecord = data.table(adjRecord) # OH data.table(adRecord)??
  
  write.table(adjClass,file="adj.csv",sep=";")
  write.table(nounClass,file="noun.csv",sep=";")
  
  
  colnames(nounRecord) = as.character(nounRecord[1,])
  colnames(adjRecord) = as.character(adjRecord[1,])
  
  save(adjRecord,file="adjhist")
  save(nounRecord,file="adjhist")
  save(logtreshold,file="logtresh")
  
  return(list(
    'nounClass' = nounClass,
    'nounRecord' = nounRecord,
    'adjClass' = adjClass,
    'adjRecord' = adjRecord,
    'myfreq_bu_red' = myfreq_bu_red,
    'history' = History,
    'edgelist' = Edgelist,
    'myfreq' = myfreq
  ))
}
