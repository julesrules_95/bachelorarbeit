load.corpus = function(corpus)
{
  if(corpus=="google"){
    # Use the new spellcorrected Google data! They have the same format as the newspaper data now.
    # Created with ../c++generic/spelling-normalization.
    #   if(OWNER=="Oliver"){  # Umlaute!!
    #     data.path = "C:\\MyFiles\\Uni\\Düsseldorf\\C10\\data\\AN-combinations\\google-trigram-unique-type-pairs-counted.txt.spellcorr"
    #   }else{
    #     data.path = GOOGLEDATA
    #   }
    data.path = GOOGLEDATA
  }else if(corpus=="newspaper"){
    data.path = NEWSPAPERDATA
  }else if(corpus=="merged"){
  }else if(corpus=="english"){
    data.path = ENGLISHDATA
  }else{
    print("INVALID CORPUS!!")
  }
  
  dnfirst = data.frame()
  if(corpus=="merged"){
    n =  read.delim(NEWSPAPERDATA, sep=";", header=TRUE, stringsAsFactors=FALSE, row.names=NULL, fileEncoding = "UTF-8")
    g = read.delim(GOOGLEDATA, sep=";", header=TRUE, stringsAsFactors=FALSE, row.names=NULL, fileEncoding = "UTF-8")
    tmp = merge(x=n,y=g, by=c("noun","adj"), all=TRUE)
    sums = rowSums(tmp[,3:4],na.rm = TRUE)
    dnfirst = data.frame(cbind(noun=tmp$noun, adj=tmp$adj, freq=sums), stringsAsFactors = FALSE)
    write.table(x=dnfirst, file="C:\\MyFiles\\Uni\\Aufsaetze\\2016\\ACL-German-adjectives\\data\\permanent\\newspaper-google-frequencies.dat",
                quote = FALSE, row.names = FALSE, sep = ";", fileEncoding = "UTF-8")
  }else{
    dnfirst = read.delim(data.path, sep=";", header=TRUE, stringsAsFactors=FALSE, row.names=NULL, fileEncoding = "UTF-8")
  }
  dnfirst$freq = as.integer(dnfirst$freq)
  
  # some more preprocessing:
  # --- items
  a = grep(pattern = "^\\-+$", dnfirst$noun) 
  if(length(a)>0){
    dnfirst = dnfirst[-a,]
  }
  # Nouns with pattern A-B --> B
  # not perfect, but OK (euro-stoxx-50 --> 50 etc.)
  a = grep(dnfirst$noun, pattern="-")
  if(length(a)>0){
    dnfirst[a,]$noun = gsub(pattern=".+\\-(.+)$", replacement = "\\1", dnfirst[a,]$noun)
  }
  # items with :
  a = grep(dnfirst$noun, pattern=":")
  if(length(a)>0){
    dnfirst[a,]$noun = sub(dnfirst[a,]$noun, pattern = ":", replacement = "_")
  }
  return(dnfirst)
}



load.thomas = function(filter.path, dnfirst)
{
  filter.adj = read.delim(filter.path, sep=";", header=TRUE, stringsAsFactors=FALSE, row.names=NULL)
  filter.adj = filter.adj[,1:2]
  if(nrow(filter.adj)>0)
  {
    # remove adj from 'thomas' that are not in dnfirst.
    W = which(!(filter.adj$adj %in% dnfirst$adj))
    if(length(W)>0){
      #print(filter.adj[W,])
      for(w in W){
        adj = filter.adj[w,]$adj
        l = nchar(adj)
        print(adj, quote=FALSE)
        ok = FALSE
        if(substr(adj,l,l)=="e"){
          tmp = substr(adj,0,l-1)
          if(length(which(dnfirst$adj==tmp))>0){
            # got a replacement
            filter.adj[w,]$adj = tmp
            ok = TRUE
            print(sprintf(" --> %s", tmp), quote=FALSE)
          }
        }
        if(!ok){
          v = grep(sprintf("^%s", adj), dnfirst$adj)
          
          if(length(v)>0){
            print(paste(unique(dnfirst[v,]$adj), collapse = " "), quote=FALSE)
          }else{
            print("  nothing", quote=FALSE)
          }
        }
      }
    }
    nold = nrow(filter.adj)
    filter.adj = filter.adj[which(filter.adj$adj %in% dnfirst$adj),]
    print(sprintf("FILTERING 'thomas': %d records --> %d", nold, nrow(filter.adj)), quote = FALSE)
    w = which(duplicated(filter.adj$adj))
    if(length(w)>0){
      filter.adj = filter.adj[-w,]
      print(sprintf(" removed %d duplicates from Thomas", length(w)), quote=FALSE)
    }
  }
  return(filter.adj)
}

# Loads Thomas' data, remove all adj. that are not in @param filter
load.thomas.filter = function(path, filter)
{
  thomas = read.delim(path, sep=";", header=TRUE, stringsAsFactors=FALSE, row.names=NULL)
  thomas = thomas[,1:2]
  if(nrow(thomas)>0)
  {
    # remove adj from 'thomas' that are not in dnfirst.
    W = which(!(thomas$adj %in% filter))
    if(length(W)>0){
      for(w in W){
        adj = thomas[w,]$adj
        l = nchar(adj)
        #print(adj, quote=FALSE)
        ok = FALSE
        if(substr(adj,l,l)=="e"){
          tmp = substr(adj,0,l-1)
          if(length(which(filter==tmp))>0){
            # got a replacement
            thomas[w,]$adj = tmp
            ok = TRUE
            #print(sprintf(" --> %s", tmp), quote=FALSE)
          }
        }
        if(!ok){
          v = grep(sprintf("^%s", adj), filter)
          if(length(v)>0){
            #print(paste(unique(filter[v]), collapse = " "), quote=FALSE)
          }else{
            #print("  nothing", quote=FALSE)
          }
        }
      }
    }
    nold = nrow(thomas)
    thomas = thomas[which(thomas$adj %in% filter),]
    #print(sprintf("FILTERING 'thomas': %d records --> %d", nold, nrow(thomas)), quote = FALSE)
    w = which(duplicated(thomas$adj))
    if(length(w)>0){
      thomas = thomas[-w,]
      #print(sprintf(" removed %d duplicates from Thomas", length(w)), quote=FALSE)
    }
  }
  return(thomas)
}
