#include <RcppEigen.h>
//#include <tuple>
#include <map>
#include <vector>
using namespace Rcpp ;
typedef Eigen::SparseMatrix<float> sparseFloatMatrix;
typedef Eigen::SparseMatrixBase<Eigen::SparseMatrix<float> > sparseFloatMatrixBase;


// [[Rcpp::depends(RcppEigen)]]
class MatrixHandler
{
	public:
	MatrixHandler(){}
	~MatrixHandler(){}
	Eigen::SparseMatrix<float> matrix;
	/**
	This one is for Kullback-Leibler, or the results of LDA.
	*/
	Eigen::MatrixXf denseMatrix;
	/**
	 * How many non-zero elements in each COLUMN of the matrix?
	 * Interpreted differently for LDA evaluation!!!!
	 */
	std::vector<int> m_nNonZeroElements;
	
	/**
	Transforms a triplet data frame into a sparse Eigen matrix.
	*/
	bool load_sparse_matrix(Rcpp::DataFrame input_triplets)
	{
		m_nNonZeroElements.resize(0);
		std::map<size_t,int> mNonZero; // index -> number of non-null elms.
		std::vector<Eigen::Triplet<float> > triplets;
		int maxrow = -1, maxcol = -1;
		IntegerVector rows = input_triplets["row"], cols = input_triplets["col"];
		NumericVector values = input_triplets["value"];
		for(unsigned int i=0;i<rows.size();i++){
			int r = rows[i], c = cols[i];
			float value = values[i];
			Eigen::Triplet<float> t(r,c,value);
			triplets.push_back(t);
			maxrow = std::max<int>(r,maxrow);
			maxcol = std::max<int>(c,maxcol);
			std::map<size_t,int>::iterator it = mNonZero.find(c);
			if(it==mNonZero.end()){
				mNonZero[(size_t)c] = 1;
			}else{
				it->second++;
			}
		}

		Rcpp::Rcout << "Matrix loaded, maxrow=" << maxrow << ", maxcol=" << maxcol << std::endl;
		if(maxrow<0 || maxcol<0){
			Rcpp::Rcout << "Invalid row or column number!" << std::endl;
			return false;
		}
		if(maxcol!=int(mNonZero.size()-1)){
			Rcpp::Rcout << " Warning: Column number != number of non-zero columns. Values:" << std::endl;
			int nout = 0;
			for(int t=0;t<maxcol;t++){
				if(mNonZero.find(t)==mNonZero.end()){
					nout++;
					Rcpp::Rcout << t << " ";
					if(nout % 20==0){Rcpp::Rcout << std::endl;}
				}
			}
			// return false;// Don't return false. This may happen during clustering!
		}
		matrix.resize((size_t)(maxrow+1), (size_t)(maxcol+1));
		matrix.setFromTriplets(triplets.begin(), triplets.end());
		m_nNonZeroElements.resize(maxcol+1); // column major!!!!! //mNonZero.size());
		for(std::map<size_t,int>::const_iterator it = mNonZero.begin(); it!=mNonZero.end(); ++it){
			size_t index = it->first;
			int n = it->second;
			if(index>=m_nNonZeroElements.size()){
				Rcpp::Rcout << "Invalid index in non-zero map: " << index << std::endl;
				//return false;
			}else{
			  m_nNonZeroElements.at(index) = n;
			}
		}
		Rcpp::Rcout << " sparse matrix loaded from data frame; dimension: " << matrix.rows() << "x" << matrix.cols() << std::endl;
		return true;
	}
	/**
	Does the actual calculation.
	*/
	Rcpp::DataFrame run_sparse(Rcpp::List params)
	{
		std::vector<std::tuple<size_t,size_t,float> >distances;
		float minIntersection = Rcpp::as<float>(params["minIntersection"]);
		float threshold = Rcpp::as<float>(params["threshold"]);
		std::string similarity_measure = Rcpp::as<std::string>(params["similarity"]);
		Rcpp::Rcout << "sim: " << similarity_measure << ", min_inter: " << minIntersection << ", threshold: " << threshold << std::endl;
		if(similarity_measure=="euclidian")
		{
			/*std::cout << coutPrefix << "Euclidean distance ... ";
			
			size_t cols = matrix.cols(), rows = matrix.rows();
			for(size_t c1=0;c1<(cols-1);c1++){
				Eigen::SparseMatrixBase<Eigen::SparseMatrix<float>>::ColXpr col1 = matrix.col(c1);
				int A = nNonZeroElements.at(c1);
				if(A>0){
					for(size_t c2=(c1+1);c2!=cols;c2++){
						int B = nNonZeroElements.at(c2);
						if(B>0){
							Eigen::VectorXf diff = col1-matrix.col(c2);
							//diff=diff.cwiseProduct(diff);
							float d = sqrt(diff.cwiseProduct(diff).sum());
							//float d = sqrt((diff*diff).sum());
							//d = 0.f; //sqrt(((col1-matrix.col(c2)) * (col1-matrix.col(c2))).sum());
							if(d < 0.5f)// TODO HACK!!!!!
							{
								distances.push_back(std::tuple<size_t,size_t,float>(c1,c2,d));
							}
						}
					}
				}
			}*/
		}
		else if(similarity_measure=="kullback"){
			// DONE in a separate function! Dense matrix!!
		}
		else if(similarity_measure=="jaccard"){
			/**
			 * Oliver, set theory!
			 * two sets A and B
			 * I = number of elements in intersection of A and B
			 * U = size of the union
			 * D = size of the symmetric difference = number of elements in A or B, but not in I
			 * D + I = U
			 * U = |A| + |B| - I
			 * =>
			 * D + I = |A| + |B| - I
			 * I = (|A| + |B| - D)/2
			 */
			Rcpp::Rcout << "Jaccard distance ... ";
			size_t cols = matrix.cols();
			for(size_t c1=0;c1<(cols-1);c1++){
				Eigen::SparseMatrixBase<Eigen::SparseMatrix<float>>::ColXpr col1 = matrix.col(c1);
				int A = m_nNonZeroElements.at(c1);
				if(A>0){
					for(size_t c2=(c1+1);c2!=cols;c2++){
						int B = m_nNonZeroElements.at(c2);
						if(B>0){
							int D = (int)(col1-matrix.col(c2)).cwiseAbs().sum(); // symmetric difference
							int I = (A+B-D)/2; // intersection
							if(I >= minIntersection){
								int U = A+B-I;
								float jaccard = float(I)/float(U);
								if(threshold==-1.f || jaccard>=threshold){
									float d = 1.f - float(I)/float(U);
									distances.push_back(std::tuple<size_t,size_t,float>(c1,c2,d));
								}
							}
						}
					}
				}
			}
			Rcpp::Rcout << "got " << distances.size() << " distances" << std::endl;
		}
		else if(similarity_measure=="jaccard-full"){
			// Same as Jaccard, but with frequency values
			/*std::cout << coutPrefix << "Jaccard distance with frequency values (from a non-binary matrix) ... ";
			size_t cols = matrix.cols(), rows = matrix.rows();
			for(size_t c1=0;c1<(cols-1);c1++){
				//Eigen::SparseMatrixBase<Eigen::SparseMatrix<float>>::ColXpr col1 = matrix.col(c1);
				Eigen::VectorXf col1 = matrix.col(c1);
				int A = nNonZeroElements.at(c1);
				if(A>0){
					for(size_t c2=(c1+1);c2!=cols;c2++){
						int B = nNonZeroElements.at(c2);
						if(B>0){
							//Eigen::SparseMatrixBase<Eigen::SparseMatrix<float>>::ColXpr 
							Eigen::VectorXf col2 = matrix.col(c2);
							size_t I = 0;
							double common = 0.0, n1 = 0.0, n2 = 0.0;
							for(size_t r=0;r!=rows;r++){
								if(col1[r]>0 && col2[r]>0){
									common+=(double)(col1[r] + col2[r]);
									I++;
								}
								n1 = col1[r]; n2+=col2[r];
							}
							if(I >= cmdline.minIntersection){
								
								double jaccard = common/(n1+n2);
								if(cmdline.threshold==-1.f || jaccard>=cmdline.threshold){
									float d = 1.f - float(jaccard);
									distances.push_back(std::tuple<size_t,size_t,float>(c1,c2,d));
								}
							}
						}
					}
				}
			}
			
			std::cout << "got " << distances.size() << " distances" << std::endl;*/
		}
		else if(similarity_measure=="manhattan"){
			/*std::cout << coutPrefix << "Manhattan distance ... ";
			size_t cols = matrix.cols();
			for(size_t c1=0;c1<(cols-1);c1++){
				//Eigen::VectorXf col1 = matrix.col(c1);
				Eigen::SparseMatrixBase<Eigen::SparseMatrix<float> >::ColXpr col1 = matrix.col(c1);
				for(size_t c2=(c1+1);c2!=cols;c2++){
					float d = (col1-matrix.col(c2)).cwiseAbs().sum();
					distances.push_back(std::tuple<size_t,size_t,float>(c1,c2,d));
				}
			}
			std::cout << "got " << distances.size() << " distances" << std::endl;*/
		}
		else{
			Rcpp::Rcout << "NYI!!" << std::endl;
		}
		return distances_to_dataframe(distances, params);
	}
	/**
	Transforms the vector of triplets into a sorted R data frame.
		\return The data frame. Can be empty.
	*/
	Rcpp::DataFrame distances_to_dataframe(std::vector<std::tuple<size_t,size_t,float>> &distances, Rcpp::List params) const
	{
		// Sort them!
		if(distances.size()>1){
			std::sort(distances.begin(), distances.end(),
				[](std::tuple<size_t,size_t,float> t1, std::tuple<size_t,size_t,float> t2){return std::get<2>(t1) < std::get<2>(t2);});
			
		}
		// Create the columns of the data frame
		unsigned int nKeep = distances.size();
		if(params.containsElementNamed("nKeep")){
			nKeep = std::min<unsigned int>(distances.size(), (unsigned int)Rcpp::as<int>(params["nKeep"]));
		}else{
			warning("Parameter 'nKeep' not set!");
		}
		Rcpp::Rcout << nKeep << std::endl;
		IntegerVector rows, cols;
		NumericVector values;
		for(unsigned int t=0;t<nKeep;t++){
			rows.push_back(std::get<0>(distances.at(t)) +1 ); // increase the row and col indices again for R!!!
			cols.push_back(std::get<1>(distances.at(t)) +1 );
			values.push_back(std::get<2>(distances.at(t)));
		}
		Rcpp::Rcout << "yo!" << std::endl;
		return DataFrame::create(_["row"]= (rows), _["col"] = (cols), _["value"] = (values));
	}
	void warning(const char* str) const
	{
		Rcpp::Rcout << "WARNING: " << str << std::endl;
	}
	void warning(const std::string &s) const { warning(s.c_str());}
	bool loadDenseMatrix(const std::string &path)
	{
		m_nNonZeroElements.resize(0);
		std::ifstream file(path, std::ios::binary);
		if(!file.good()){
			warning("Could not open this file: " + path);
			return false;
		}
		std::string line;
		
		std::vector<std::vector<float> > data;
		size_t nvars = 0;
		while(std::getline(file, line)){
			while(line.at(line.size()-1)==10 || line.at(line.size()-1)==13){
				line = line.substr(0, line.size()-1);
			}
			if(!line.empty()){
				std::stringstream s(line);
				std::vector<float> record;
				float fmax = -1.f;
				while(!s.eof()){
					float f;
					s >> f;
					fmax = std::max<float>(f,fmax);
					record.push_back(f);
				}
				if(nvars==0){
					nvars = record.size();
				}else{
					if(nvars!=record.size()){
						std::wcout << "Invalid record size in input file: " << record.size() << " instead of " << nvars << std::endl;
						return 1;
					}
				}
				data.push_back(record);
				m_nNonZeroElements.push_back(1);
			}
		}

		file.close();
		if(data.empty()){
			warning("Invalid row or column number!");
			return false;
		}

		// This loop distributes more weight to initially higher values.
		//std::vector<float> t1 = data.at(0), t5 = data.at(5);
		for(int loop=0;loop<3;loop++){
			std::vector<float> csums(nvars, 0.f);
			for(size_t var=0;var<nvars;var++){
				for(size_t row=0;row<data.size();row++){
					//csums.at(var)+=data.at(row).at(var);
					csums.at(var)+=pow(data.at(row).at(var),1.5f);
				}
			}
			for(size_t var=0;var<nvars;var++){
				for(size_t row=0;row<data.size();row++){
					//data.at(row).at(var)/=csums.at(var);
					data.at(row).at(var)=pow(data.at(row).at(var),1.5f)/csums.at(var);
				}
			}
			for(size_t row=0;row<data.size();row++){
				float sum = 0.f;
				for(size_t var=0;var!=nvars;var++){
					//sum+=pow(data.at(row).at(var),2.f);
					sum+=data.at(row).at(var);
				}
				for(size_t var=0;var!=nvars;var++){
					//data.at(row).at(var)=pow(data.at(row).at(var),2.f)/sum;
					data.at(row).at(var)/=sum;
				}
			}
		}


		denseMatrix.resize(nvars, data.size()); // column major!
		for(size_t i=0;i<data.size();i++){
			std::vector<float>::const_pointer col = &data.at(i)[0];
			for(size_t j=0;j<nvars;j++){
				denseMatrix(j,i) = col[j];
			}
		}

		return true;
	}
};

// [[Rcpp::export]]
Rcpp::DataFrame matrix_distance(Rcpp::DataFrame triplets, Rcpp::List params)
{
/**
This function gets a data frame from R, transforms it into a sparse Eigen matrix,
and calculates column-wise distances.
*/
	Rcpp::DataFrame result;
	MatrixHandler handler;
	if(!handler.load_sparse_matrix(triplets)){
		return result;
	}
	result = handler.run_sparse(params);
	return result;
  
  /*
  float minIntersection = 0.1f;
  float threshold = 0.1f;
  std::cout << "Jaccard distance ... ";
  //std::vector<std::tuple<size_t,size_t,float>> distances;
  std::vector<unsigned int> idx1, idx2;
  std::vector<float> distances;
  size_t cols = M.cols();
  for(size_t c1=0;c1<(cols-1);c1++){
    sparseFloatMatrixBase::ColXpr col1 = M.col(c1);
    //int A = nNonZeroElements.at(c1);
    //if(A>0)
    {
      for(size_t c2=(c1+1);c2!=cols;c2++){
        //int B = nNonZeroElements.at(c2);
        //if(B>0)
        {
          int D = (int)(col1-M.col(c2)).cwiseAbs().sum(); // symmetric difference
          int I = 20; // (A+B-D)/2; // intersection
          if(I >= minIntersection){
            int U = 15; // A+B-I;
            float jaccard = float(I)/float(U);
            if(threshold==-1.f || jaccard>=threshold){
              float d = 1.f - float(I)/float(U);
              //distances.push_back(std::tuple<size_t,size_t,float>(c1,c2,d));
              idx1.push_back(c1); idx2.push_back(c2);
              distances.push_back(d);
            }
          }
        }
      }
    }
  }
  std::cout << "got " << distances.size() << " distances" << std::endl;
  */
}
