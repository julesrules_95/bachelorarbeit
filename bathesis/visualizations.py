import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots

import pyLDAvis
import pyLDAvis.gensim  # don't skip this

def plot_find_best_k(n_clusters, coh, chs, dbs, ssr=None):
    """
    simple plot consisting of two line-charts visualizing the 
    ellbow-method and the cohesion-method; only used in supervised 
    mode

    :param n_clusters: list containing the potential numbers of best-k
    :param ssr: list containing the ssr-score (sum of squared errors)
    :param coh: list containing the cohesion scores
    :param chs: list containing the Calinski Harabasz Score
    :param dbs: list containing the Davies Bouldin Score
    """
    fig = make_subplots(
        specs=[[{"secondary_y": True}]]
    )

    if ssr: 
        fig.add_trace(
            go.Scatter(
                x=n_clusters,
                y=ssr,
                mode='lines',
                name='SSE',
                hovertemplate = "<b> %{y} </b> <br>number of clusters: %{x} <extra></extra>"
            ), secondary_y=False,
        )

    fig.add_trace(
        go.Scatter(
            x=n_clusters,
            y=coh,
            mode='lines',
            name='Cohesion',
            hovertemplate = "<b> %{y} </b> <br>number of clusters: %{x} <extra></extra>"
        ), secondary_y=True,
    )

    fig.add_trace(
        go.Scatter(
            x=n_clusters,
            y=chs,
            mode='lines',
            name='Calinski Harabasz Score',
            hovertemplate = "<b> %{y} </b> <br>number of clusters: %{x} <extra></extra>"
        ), secondary_y=False,
    )

    fig.add_trace(
        go.Scatter(
            x=n_clusters,
            y=dbs,
            mode='lines',
            name='Davies Bouldin Score',
            hovertemplate = "<b> %{y} </b> <br>number of clusters: %{x} <extra></extra>"
        ), secondary_y=True,
    )

    fig.update_layout(
        margin=dict(l=20, r=20, t=45, b=20),
        showlegend=True,
        title_text="Plots for choosing the best number of clusters",
    )

    # Set x-axis title
    fig.update_xaxes(title_text="number of clusters")

    # Set y-axes titles
    fig.update_yaxes(title_text="<b>Ellbow</b> yaxis", secondary_y=False)
    fig.update_yaxes(title_text="<b>Cohesion</b> yaxis", secondary_y=True)
    
    fig.show()

def simple_plot(n_clusters, coh):
    fig = px.line(x=n_clusters, y=coh, title="Coherence")
    fig.show()

def plot_lda(lda_model, corpus, id2word):
    """
    """
    vis = pyLDAvis.gensim.prepare(lda_model, corpus, id2word, mds='mmds')
    pyLDAvis.show(vis)