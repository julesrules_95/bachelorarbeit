import sys
import json
import pandas as pd
from pathlib import Path

import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

import plotly.express as px
import plotly.graph_objects as go

from bathesis.app.app import app

def vis(kind):
    """
    this function is used to create the layout of the clustering visualizations 
    based on a given kind of resource and clustering method by first loading the 
    specified data (kind&method) which is then used to create the layout

    :param kind: the kind of resource to be visualized

    :returns:
       tuple containing six elements:
          * layout: the dash component of the whole visualization
          * df_a: the dataframe of adjectives to be used
          * df_n: the dataframe of nouns to be used
          * df_tfidf: the dataframe of tf-idf scores to be used
          * best_k_a: the best number of clusters for clustering adjectives
          * best_k_n: the best number of clusters for clustering nouns
    """
    # load preprocessed data all at once to speed up the whole app
    try:
        with open(str(Path(__file__).parent.parent.parent) + '/static/clustered/bidirectional/{}/df_a.csv'.format(kind)) as f:
            df_a = pd.read_csv(f, index_col=0)
        with open(str(Path(__file__).parent.parent.parent) + '/static/clustered/bidirectional/{}/df_n.csv'.format(kind)) as f:
            df_n = pd.read_csv(f, index_col=0)
        with open(str(Path(__file__).parent.parent.parent) + '/static/clustered/bidirectional/{}/df_subclasses_adj.csv'.format(kind)) as f:
            df_subclasses_a = pd.read_csv(f, index_col=0)
        with open(str(Path(__file__).parent.parent.parent) + '/static/clustered/bidirectional/{}/df_subclasses_n.csv'.format(kind)) as f:
            df_subclasses_n = pd.read_csv(f, index_col=0)
    except FileNotFoundError:
        sys.exit("The files you wanna visualize are either not unzipped yet or not even clustered!")
    clusters_a = set(df_a['class'])
    n_a = max(clusters_a)
    clusters_n = set(df_a['class'])
    n_n = max(clusters_n)
    clusters = sorted(list(clusters_a.union(clusters_n)))
    words = list(df_a.index)+list(df_n.index)

    layout = html.Div(children=[
        dbc.Row(dbc.Col([dbc.Jumbotron([
            html.H1(children='Visualizations of bidirectional clustering on {}'.format(kind)),
            html.P('This part of the dashboard shows you the results of bidirectional clustering in two parts: ', className="lead"),
            html.Ol([
                html.Li('the clusters by cluster name on the left'),
                html.Li('the clusters by a chosen word on the right')
            ])
        ])])),
        dbc.Row(dbc.Col([
            html.H2(children='Single cluster'),
            html.P('''This scatter plot shows the words of the chosen cluster as well as the names of 
                all clusters that are subclasses of this cluster.''', className="lead")
        ]), style={'margin-left': '0px', 'margin-right': '0px'}),
        dbc.Row([
            dbc.Col([
                html.H3(id='bidi-cluster-heading'),
                dcc.Dropdown(
                    id='bidi-select',
                    options=[{'label': 'Cluster {}'.format(i), 'value': i} for i in clusters],
                    value=clusters[0],
                    placeholder="Choose a cluster",
                ),
                dcc.Graph(
                    id='bidi-cluster'
                )
            ]),
            dbc.Col([
                html.H3(children='Choose a word to see in its cluster'),
                dcc.Dropdown(
                    id='word-bidi-select',
                    options=[{'label': i, 'value': i} for i in words],
                    value=words[0],
                    placeholder="Select a word"
                ), 
                dcc.Graph(
                    id='bidi-words'
                )
            ])
        ], style={'margin-left': '0px', 'margin-right': '0px'})
    ])
    return layout, df_a, df_n, df_subclasses_a, df_subclasses_n, n_a, n_n