import sys
import json
import pandas as pd
from pathlib import Path

import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

import plotly.express as px
import plotly.graph_objects as go

from bathesis.app.app import app

def vis(kind, method):
    """
    this function is used to create the layout of the clustering visualizations 
    based on a given kind of resource and clustering method by first loading the 
    specified data (kind&method) which is then used to create the layout

    :param kind: the kind of resource to be visualized
    :param method: the clustering method to be visualized

    :returns:
       tuple containing six elements:
          * layout: the dash component of the whole visualization
          * df_a: the dataframe of adjectives to be used
          * df_n: the dataframe of nouns to be used
          * df_tfidf: the dataframe of tf-idf scores to be used
          * best_k_a: the best number of clusters for clustering adjectives
          * best_k_n: the best number of clusters for clustering nouns
    """
    # load preprocessed data all at once to speed up the whole app
    try:
        with open(str(Path(__file__).parent.parent.parent) + '/static/clustered/{}/{}/df_a.csv'.format(method, kind)) as f:
            df_a = pd.read_csv(f, index_col=0)
        with open(str(Path(__file__).parent.parent.parent) + '/static/clustered/{}/{}/df_n.csv'.format(method, kind)) as f:
            df_n = pd.read_csv(f, index_col=0)
        with open(str(Path(__file__).parent.parent.parent) + '/static/clustered/{}/{}/df_tfidf.csv'.format(method, kind)) as f:
            df_tfidf = pd.read_csv(f, index_col=0)
            df_tfidf.columns = df_tfidf.columns.astype(int)
        with open(str(Path(__file__).parent.parent.parent) + '/static/clustered/{}/{}/dict.json'.format(method, kind)) as f:
            cluster_dict = json.load(f)
        best_k_a = cluster_dict['best_k_a']
        best_k_n = cluster_dict['best_k_n']
    except FileNotFoundError:
        sys.exit("The files you wanna visualize are either not unzipped yet or not even clustered!")

    if method == 'kmeans':
        cluster_method = 'K-Means'
    else:
        cluster_method = 'Agglomerative'

    clusters = list(range(best_k_a+best_k_n))
    words = list(df_a.index)+list(df_n.index)

    kmeans_adj = px.scatter_3d(df_a, x="x", y="y", z='z', color="cluster", hover_data=[df_a.index])
    kmeans_adj.update_traces(marker=dict(size=5))
    kmeans_adj.update_layout(xaxis={'title_text': None, 'showticklabels': False, 'showgrid':False, 'zeroline':False} , yaxis={'title_text': None, 'showticklabels': False, 'showgrid':False, 'zeroline':False})
    kmeans_n = px.scatter_3d(df_n, x="x", y="y", z='z', color="cluster", hover_data=[df_n.index])
    kmeans_n.update_traces(marker=dict(size=5))
    kmeans_n.update_layout(xaxis={'title_text': None, 'showticklabels': False, 'showgrid':False, 'zeroline':False} , yaxis={'title_text': None, 'showticklabels': False, 'showgrid':False, 'zeroline':False})

    layout = html.Div(children=[
        # the title of the page
        dbc.Row(dbc.Col([dbc.Jumbotron([
            html.H1(children='Visualizations of {} clustering on {}'.format(cluster_method, kind)),
            html.P('This dashboard comes in three parts:', className="lead"),
            html.Ol([
                html.Li('the overall visualizations of the results'),
                html.Li('the most relevant clusters depending on the cluster chosen'),
                html.Li('the words most likely to modify or be modified by a given word')
            ])
        ])])),
        dbc.Row(dbc.Col(dbc.Tabs([
            # the raw kmeans vis
            dbc.Tab(label='Overall Clustering', children=[
                dbc.Row(
                    dbc.Col([
                        html.H2(children='{} clustering using {} clusters'.format(cluster_method, best_k_a+best_k_n)),
                        html.P('''These charts show the results of the {} clustering of 
                            adjectives on the left and nouns on the right'''.format(cluster_method), className="lead")
                    ]), style={'margin-left': '0px', 'margin-right': '0px', 'margin-top': '10px'}
                ),
                dbc.Row([
                    dbc.Col([
                        html.H3(children='Clustering of adjectives using {} clusters'.format(best_k_a)),
                        dcc.Graph(
                            id='kmeans-adj',
                            figure=kmeans_adj
                        )
                    ]),
                    dbc.Col([
                        html.H3(children='Clustering of nouns using {} clusters'.format(best_k_n)),
                        dcc.Graph(
                            id='kmeans-n',
                            figure=kmeans_n
                        )
                    ])
                ], style={'margin-left': '0px', 'margin-right': '0px'})
            ]),
            dbc.Tab(label='Correlated Clusters', children=[
                # the vis of related clusters
                dbc.Row(dbc.Col([
                    html.H2(children='Correlated clusters'),
                    html.P('''These charts show the chosen cluster on the left and the 
                        most relevant correlated clusters on the right''', className="lead")
                ]), style={'margin-left': '0px', 'margin-right': '0px'}),
                dbc.Row([
                    dbc.Col([
                        html.H3(id='single-cluster-heading'),
                        dcc.Dropdown(
                            id='cluster-select',
                            options=[{'label': 'Cluster {}'.format(i), 'value': i} for i in clusters],
                            value=clusters[0],
                            placeholder="Choose a cluster",
                        ),
                        dcc.Graph(
                            id='cluster-single'
                        ),  
                    ]),
                    dbc.Col([
                        html.H3(children='The most relevant correlated clusters'),
                        dcc.Graph(
                            id='cor-clusters'
                        ),  
                        dcc.Slider(
                            id='cluster-slider',
                            min=min(list(range(1,30))),
                            max=max(list(range(1,30))),
                            value=10,
                            step=1,
                            tooltip = {"always_visible": False, "placement": "top"}
                        )
                    ])
                ], style={'margin-left': '0px', 'margin-right': '0px'})
            ]),
            dbc.Tab(label='Correlated Words', children=[
                # the visualization of the most likely words to be related to a given word
                dbc.Row(dbc.Col([
                    html.H2(children='Most likely correlated words'.format()),
                    html.P('''These charts show the frequency of the chosen word in its own cluster on the 
                        left and the most likely words for it to appear with on the right''', className="lead")
                ]), style={'margin-left': '0px', 'margin-right': '0px'}),
                dbc.Row([
                    dbc.Col([
                        html.H3(children='Choose a word to get suggestions for'.format()),
                        dcc.Dropdown(
                            id='word-select',
                            options=[{'label': i, 'value': i} for i in words],
                            value=words[0],
                            placeholder="Select a word"
                        ), 
                        dcc.Graph(
                            id='word-stats'
                        )
                    ]),
                    dbc.Col([
                        html.H3(children='The most likely words to appear with'.format()),
                        dcc.Graph(
                            id='cor-words'
                        ),   
                        dcc.Slider(
                            id='word-slider',
                            min=min(list(range(1,50))),
                            max=max(list(range(1,50))),
                            value=10,
                            tooltip = {"always_visible": False, "placement": "top"},
                            step=1
                        ),
                    ]),
                ], style={'margin-left': '0px', 'margin-right': '0px'})
            ])
        ])))
    ])
    return layout, df_a, df_n, df_tfidf, best_k_a, best_k_n