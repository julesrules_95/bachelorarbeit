import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

from bathesis.app.app import app

def vis():
    """
    this function creates the home-page of the dashboard consisting of different 
    cells that contain the chosen parameters for each part of the project as well 
    as the links to the used sources and the relevant visualization

    :returns: layout: the layout to be shown in the dashboard
    """
    layout = html.Div(children=[
        # the title of the page
        dbc.Row(dbc.Col([dbc.Jumbotron([
            html.H1(children="Welcome to the bachelor thesis' dashboard"),
            html.P('Here you can view the results for:', className="lead"),
            html.Ol([
                html.Li('kmeans clustering'),
                html.Li('agglomerative clustering'),
                html.Li('bidirectional clustering'),
                html.Li('the evaluation of clustering methods')
            ]),
            html.P('''Below you will find further information on the clustering 
                methods and their parameters:''', className="lead")
        ])])),
        dbc.Row([
            dbc.Col(dbc.Card([
                dbc.CardHeader(html.H4("Word2Vec", className="card-title"), 
                    style={'background-color':'#dc3545', 'color': 'white'}),
                dbc.CardBody([
                    html.H5("Used to convert words to vectors", style={'font-style': 'italic'}),
                    html.P("The parameters chosen: "),
                    html.Ul([
                        html.Li('size: 300'),
                        html.Li('alpha: 0.025'),
                        html.Li('window: 3'),
                        html.Li('min_count: '),
                        html.Ul([
                            html.Li('all: 15'),
                            html.Li('articles: 2'),
                            html.Li('comments: 10'),
                            html.Li('recipes: 10')
                        ]),
                        html.Li('workers: 3'),
                        html.Li('sg: 0'),
                        html.Li('sample: 0.001'),
                        html.Li('seed: 1234'),
                        html.Li('hs: 0'),
                        html.Li('negative: 5'),
                        html.Li('ns_exponent: 0.25'),
                        html.Li('cbow_mean: 0'),
                        html.Li('iter: 10'),
                    ]),
                ]),
                dbc.CardFooter(dbc.Button("Source", style={'background-color':'#dc3545', 'border-color':'#dc3545', 
                    'color': 'white', 'margin-right':'10px'}, href="https://radimrehurek.com/gensim/models/word2vec.html")),
            ])),
            dbc.Col(dbc.Card([
                dbc.CardHeader(html.H4("TSNE", className="card-title"), 
                    style={'background-color':'#fd7e14', 'color': 'white'}),
                dbc.CardBody([
                    html.H5("Used to reduce dimensionality", style={'font-style': 'italic'}),
                    html.P("The parameters chosen: "),
                    html.Ul([
                        html.Li('n_components: 3'),
                        html.Li('perplexity: 30'),
                        html.Li('n_iter: 1000'),
                        html.Li('random_state: 23')
                    ]),
                ]),
                dbc.CardFooter(dbc.Button("Source", style={'background-color':'#fd7e14', 'border-color':'#fd7e14', 
                    'color': 'white', 'margin-right':'10px'}, href="https://scikit-learn.org/stable/modules/generated/sklearn.manifold.TSNE.html?highlight=tsne#sklearn.manifold.TSNE")),
            ])),
            dbc.Col(dbc.Card([
                dbc.CardHeader(html.H4("K-means Clustering", className="card-title"), 
                    style={'background-color':'#ffc107', 'color': 'white'}),
                dbc.CardBody([
                    html.H5("First clustering method", style={'font-style': 'italic'}),
                    html.H6("Parameters chosen: ", className="card-subtitle"),
                    html.Ul([
                        html.Li('n_clusters: '),
                        html.Ul([
                            html.Li('fast: [2,5,8,10,13,16,20,25,30,40,80,100,130,160,200,230,260,300,350,400,450,500]'),
                            html.Li('extensive: list from 1 to the number of nouns/adjectives in the dataset')
                        ]),
                        html.Li('best_k (the best n_clusters chosen): '),
                        html.Ul([
                            html.Li('all: adjectives: 551; nouns: 1272'),
                            html.Li('articles: adjectives: 44; nouns: 422'),
                            html.Li('comments: adjectives: 509; nouns: 1062'),
                            html.Li('recipes: adjectives: 374; nouns: 855')
                        ]),
                        html.Li('n_init: 10'),
                        html.Li('max_iter: 900'),
                        html.Li('random_state: 42')
                    ]),
                ]),
                dbc.CardFooter([
                    dbc.Button("Source", style={'background-color':'#ffc107', 'border-color':'#ffc107', 
                        'color': 'white', 'margin-right':'10px'}, href="https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html?highlight=kmeans#sklearn.cluster.KMeans"),
                    dbc.Button("Visualization", style={'background-color':'#ffc107', 'border-color':'#ffc107', 
                        'color': 'white', 'margin-right':'10px'}, href="http://127.0.0.1:8050/vis/kmeans_recipes")
                ]),
            ])),
        ], style={'margin-left': '0px', 'margin-right': '0px', 'margin-top':'10px', 'margin-bottom':'10px'}),
        dbc.Row([
            dbc.Col(dbc.Card([
                dbc.CardHeader(html.H4("Agglomerative Clustering", className="card-title"), 
                    style={'background-color':'#28a745', 'color': 'white'}),
                dbc.CardBody([
                    html.H5("Second clustering method", style={'font-style': 'italic'}),
                    html.P("Parameters chosen: "),
                    html.Ul([
                        html.Li('distance_threshold: '),
                        html.Ul([
                            html.Li('all: 2'),
                            html.Li('articles: 25'),
                            html.Li('comments: 2'),
                            html.Li('recipes: 2')
                        ]),
                        html.Li('linkage: average')
                    ]),
                ]),
                dbc.CardFooter([
                    dbc.Button("Source", style={'background-color':'#28a745', 'border-color':'#28a745', 
                        'color': 'white', 'margin-right':'10px'}, href="https://scikit-learn.org/stable/modules/generated/sklearn.cluster.AgglomerativeClustering.html?highlight=agglo#sklearn.cluster.AgglomerativeClustering"),
                    dbc.Button("Visualization", style={'background-color':'#28a745', 'border-color':'#28a745', 
                        'color': 'white', 'margin-right':'10px'}, href="http://127.0.0.1:8050/vis/aggl_recipes")
                ])
            ])),
            dbc.Col(dbc.Card([
                dbc.CardHeader(html.H4("Bidirectional Clustering", className="card-title"), 
                    style={'background-color':'#007bff', 'color': 'white'}),
                dbc.CardBody([
                    html.H5("Clustering method to compare with", style={'font-style': 'italic'}),
                    html.P("Parameters chosen: "),
                    html.Ul([
                        html.Li('v_n_non_sparse: '),
                        html.Ul([
                            html.Li('all: 5'),
                            html.Li('articles: 1'),
                            html.Li('comments: 5'),
                            html.Li('recipes: 4')
                        ]),
                        html.Li('v_p: 0.1'),
                        html.Li('v_q: 0.05'),
                        html.Li('binary merge method: majority'), 
                        html.Li('similarity measure: jaccard')
                    ]),
                ]),
                dbc.CardFooter([
                    dbc.Button("Source", style={'background-color':'#007bff', 'border-color':'#007bff', 
                        'color': 'white', 'margin-right':'10px'}, href=""),
                    dbc.Button("Visualization", style={'background-color':'#007bff', 'border-color':'#007bff', 
                        'color': 'white', 'margin-right':'10px'}, href="")
                ]),
            ])),
            dbc.Col(dbc.Card([
                dbc.CardHeader(html.H4("Evaluation", className="card-title"), 
                    style={'background-color':'#6610f2', 'color': 'white'}),
                dbc.CardBody([
                    html.H5("Comparing results of clustering to Gold-Standard", style={'font-style': 'italic'}),
                    html.P("Parameters chosen: "),
                    html.Ul([
                        html.Li('number of words used to evaluate:'),
                        html.Ul([
                            html.Li('all: adjectives: 80, nouns: 104'),
                            html.Li('articles: adjectives: 94, nouns: 97'),
                            html.Li('comments: adjectives: 77, nouns: 98'),
                            html.Li('recipes: adjectives: 80, nouns: 84')
                        ]),
                    ]),
                ]),
                dbc.CardFooter([
                    dbc.Button("Source", style={'background-color':'#6610f2', 'border-color':'#6610f2', 
                        'color': 'white', 'margin-right':'10px'}, href=""),
                    dbc.Button("Visualization", style={'background-color':'#6610f2', 'border-color':'#6610f2', 
                        'color': 'white', 'margin-right':'10px'}, href="")
                    ]),
            ]))
        ], style={'margin-left': '0px', 'margin-right': '0px', 'margin-top':'10px', 'margin-bottom':'10px'})
    ])
    return layout