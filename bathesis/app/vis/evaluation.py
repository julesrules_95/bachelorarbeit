import sys
import json
import pandas as pd
from pathlib import Path

import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

import plotly.express as px
import plotly.graph_objects as go

from bathesis.app.app import app

def vis(id):
    """
    this function is used to create the layout of the clustering visualizations 
    based on a given kind of resource and clustering method by first loading the 
    specified data (kind&method) which is then used to create the layout

    :param kind: the kind of resource to be visualize
    """
    with open(str(Path(__file__).parent.parent.parent) + '/static/evaluation/{}/aggl-a-eval.json'.format(id)) as f:
        aggl_a = json.load(f)
    with open(str(Path(__file__).parent.parent.parent) + '/static/evaluation/{}/aggl-n-eval.json'.format(id)) as f:
        aggl_n = json.load(f)
    with open(str(Path(__file__).parent.parent.parent) + '/static/evaluation/{}/aggl-eval.json'.format(id)) as f:
        aggl = json.load(f)
    with open(str(Path(__file__).parent.parent.parent) + '/static/evaluation/{}/bidirectional-a-eval.json'.format(id)) as f:
        bidi_a = json.load(f)
    with open(str(Path(__file__).parent.parent.parent) + '/static/evaluation/{}/bidirectional-n-eval.json'.format(id)) as f:
        bidi_n = json.load(f)
    with open(str(Path(__file__).parent.parent.parent) + '/static/evaluation/{}/bidirectional-eval.json'.format(id)) as f:
        bidi = json.load(f)
    with open(str(Path(__file__).parent.parent.parent) + '/static/evaluation/{}/kmeans-a-eval.json'.format(id)) as f:
        kmeans_a = json.load(f)
    with open(str(Path(__file__).parent.parent.parent) + '/static/evaluation/{}/kmeans-n-eval.json'.format(id)) as f:
        kmeans_n = json.load(f)
    with open(str(Path(__file__).parent.parent.parent) + '/static/evaluation/{}/kmeans-eval.json'.format(id)) as f:
        kmeans = json.load(f)

    # prepare dfs for the plots 
    df_eval = pd.DataFrame({'method':['rand-index', 'precision', 'recall'], 
        'aggl': [aggl['ri'], aggl['p'], aggl['r']],
        'bidi': [bidi['ri'], bidi['p'], bidi['r']],
        'kmeans': [kmeans['ri'], kmeans['p'], kmeans['r']]})
    df_eval_a = pd.DataFrame({'method':['rand-index', 'precision', 'recall'], 
        'aggl': [aggl_a['ri'], aggl_a['p'], aggl_a['r']],
        'bidi': [bidi_a['ri'], bidi_a['p'], bidi_a['r']],
        'kmeans': [kmeans_a['ri'], kmeans_a['p'], kmeans_a['r']]})
    df_eval_n = pd.DataFrame({'method':['rand-index', 'precision', 'recall'], 
        'aggl': [aggl_n['ri'], aggl_n['p'], aggl_n['r']],
        'bidi': [bidi_n['ri'], bidi_n['p'], bidi_n['r']],
        'kmeans': [kmeans_n['ri'], kmeans_n['p'], kmeans_n['r']]})
    df_a = pd.DataFrame({'totals':['tp', 'tn', 'fp', 'fn'], 
        'aggl': [aggl_a['tp'], aggl_a['tn'], aggl_a['fp'], aggl_a['fn']],
        'bidi': [bidi_a['tp'], bidi_a['tn'], bidi_a['fp'], bidi_a['fn']],
        'kmeans': [kmeans_a['tp'], kmeans_a['tn'], kmeans_a['fp'], kmeans_a['fn']]})
    df_n = pd.DataFrame({'totals':['tp', 'tn', 'fp', 'fn'], 
        'aggl': [aggl_n['tp'], aggl_n['tn'], aggl_n['fp'], aggl_n['fn']],
        'bidi': [bidi_n['tp'], bidi_n['tn'], bidi_n['fp'], bidi_n['fn']],
        'kmeans': [kmeans_n['tp'], kmeans_n['tn'], kmeans_n['fp'], kmeans_n['fn']]})
    df_all = pd.DataFrame({'totals':['tp', 'tn', 'fp', 'fn'], 
        'aggl': [aggl['tp'], aggl['tn'], aggl['fp'], aggl['fn']],
        'bidi': [bidi['tp'], bidi['tn'], bidi['fp'], bidi['fn']],
        'kmeans': [kmeans['tp'], kmeans['tn'], kmeans['fp'], kmeans['fn']]})

    # prepare the plots
    eval_all = go.Figure(data=[
        go.Bar(name='Agglomerative', x=df_eval.method, y=df_eval.aggl),
        go.Bar(name='Bidirectional', x=df_eval.method, y=df_eval.bidi),
        go.Bar(name='Kmeans', x=df_eval.method, y=df_eval.kmeans)])
    eval_all.update_layout(barmode='group')
    eval_all.update_yaxes(type="log")
    eval_a = go.Figure(data=[
        go.Bar(name='Agglomerative', x=df_eval_a.method, y=df_eval_a.aggl),
        go.Bar(name='Bidirectional', x=df_eval_a.method, y=df_eval_a.bidi),
        go.Bar(name='Kmeans', x=df_eval_a.method, y=df_eval_a.kmeans)])
    eval_a.update_layout(barmode='group')
    eval_a.update_yaxes(type="log")
    eval_n = go.Figure(data=[
        go.Bar(name='Agglomerative', x=df_eval_n.method, y=df_eval_n.aggl),
        go.Bar(name='Bidirectional', x=df_eval_n.method, y=df_eval_n.bidi),
        go.Bar(name='Kmeans', x=df_eval_n.method, y=df_eval_n.kmeans)])
    eval_n.update_layout(barmode='group')
    eval_n.update_yaxes(type="log")
    total_a = go.Figure(data=[
        go.Bar(name='Agglomerative', x=df_a.totals, y=df_a.aggl),
        go.Bar(name='Bidirectional', x=df_a.totals, y=df_a.bidi),
        go.Bar(name='Kmeans', x=df_a.totals, y=df_a.kmeans)])
    total_a.update_layout(barmode='group')
    total_a.update_yaxes(type="log")
    total_n = go.Figure(data=[
        go.Bar(name='Agglomerative', x=df_n.totals, y=df_n.aggl),
        go.Bar(name='Bidirectional', x=df_n.totals, y=df_n.bidi),
        go.Bar(name='Kmeans', x=df_n.totals, y=df_n.kmeans)])
    total_n.update_layout(barmode='group')
    total_n.update_yaxes(type="log")
    total_all = go.Figure(data=[
        go.Bar(name='Agglomerative', x=df_all.totals, y=df_all.aggl),
        go.Bar(name='Bidirectional', x=df_all.totals, y=df_all.bidi),
        go.Bar(name='Kmeans', x=df_all.totals, y=df_all.kmeans)])
    total_all.update_layout(barmode='group')
    total_all.update_yaxes(type="log")

    layout = html.Div(children=[

        dbc.Row(dbc.Col([dbc.Jumbotron([
            html.H1(children='Visualizations of the evaluation of clustering methods on {}'.format(id)),
            html.P('This part of the dashboard shows you the evaluation of the clustering methods: ', className="lead"),
            html.Ol([
                html.Li('first the evaluation of clustering the adjectives with the rand index on the left and the total numbers on the right'),
                html.Li('first the evaluation of clustering the nouns with the rand index on the left and the total numbers on the right')
            ])
        ])])),
        
        dbc.Row(dbc.Col(dbc.Tabs([
            # the overview
            dbc.Tab(label='Overview', children=[
                dbc.Row(dbc.Col([
                    html.H2(children='Overview over all evaluation scores')
                ]), style={'margin-left': '0px', 'margin-right': '0px'}),
                
                dbc.Row([
                    dbc.Col([
                        html.H3(children='Comparison of the scores'),
                        dcc.Graph(
                            figure=eval_all
                        )
                    ]),
                    dbc.Col([
                        html.H3(children='Comparison of total numbers'),
                        dcc.Graph(
                            figure=total_all
                        )
                    ])
                ], style={'margin-left': '0px', 'margin-right': '0px'}),
            ]),

            dbc.Tab(label='Individual', children=[
                dbc.Row(dbc.Col([
                    html.H2(children='Visualization of the results per word class')
                ]), style={'margin-left': '0px', 'margin-right': '0px'}),

                dbc.Row(dbc.Col([
                html.H2(children='Evaluation of adjectives')
                ]), style={'margin-left': '0px', 'margin-right': '0px'}),
                
                dbc.Row([
                    dbc.Col([
                        html.H3(children='Comparison of the scores'),
                        dcc.Graph(
                            figure=eval_a
                        )
                    ]),
                    dbc.Col([
                        html.H3(children='Comparison of total numbers'),
                        dcc.Graph(
                            figure=total_a
                        )
                    ])
                ], style={'margin-left': '0px', 'margin-right': '0px'}),

                dbc.Row(dbc.Col([
                    html.H2(children='Evaluation of nouns')
                ]), style={'margin-left': '0px', 'margin-right': '0px'}),

                dbc.Row([
                    dbc.Col([
                        html.H3(children='Comparison of the scores'),
                        dcc.Graph(
                            figure=eval_n
                        )
                    ]),
                    dbc.Col([
                        html.H3(children='Comparison of total numbers'),
                        dcc.Graph(
                            figure=total_n
                        )
                    ])
                ], style={'margin-left': '0px', 'margin-right': '0px'})
                ])
        ])))

    ])

    return layout