import dash_core_components as dcc
import dash_bootstrap_components as dbc

def Navbar():
    navbar = dbc.NavbarSimple(
        children=[
            dcc.Location(id="url", pathname="/home"),
            dbc.NavItem(dbc.NavLink("Home", href="/home")),
            dbc.DropdownMenu(
                children=[
                    dbc.DropdownMenuItem("All", href="/vis/kmeans_all"),
                    dbc.DropdownMenuItem("Articles", href="/vis/kmeans_articles"),
                    dbc.DropdownMenuItem("Comments", href="/vis/kmeans_comments"),
                    dbc.DropdownMenuItem("Recipes", href="/vis/kmeans_recipes"),
                ],
                nav=True,
                in_navbar=True,
                label="Kmeans clustering",
            ),
            dbc.DropdownMenu(
                children=[
                    dbc.DropdownMenuItem("All", href="/vis/aggl_all"),
                    dbc.DropdownMenuItem("Articles", href="/vis/aggl_articles"),
                    dbc.DropdownMenuItem("Comments", href="/vis/aggl_comments"),
                    dbc.DropdownMenuItem("Recipes", href="/vis/aggl_recipes"),
                ],
                nav=True,
                in_navbar=True,
                label="Agglomerative Clustering",
            ),
            dbc.DropdownMenu(
                children=[
                    dbc.DropdownMenuItem("All", href="/vis/bidi_all"),
                    dbc.DropdownMenuItem("Articles", href="/vis/bidi_articles"),
                    dbc.DropdownMenuItem("Comments", href="/vis/bidi_comments"),
                    dbc.DropdownMenuItem("Recipes", href="/vis/bidi_recipes"),
                ],
                nav=True,
                in_navbar=True,
                label="Bidirectional clustering",
            ),
            dbc.DropdownMenu(
                children=[
                    dbc.DropdownMenuItem("All", href="/vis/eval_all"),
                    dbc.DropdownMenuItem("Articles", href="/vis/eval_articles"),
                    dbc.DropdownMenuItem("Comments", href="/vis/eval_comments"),
                    dbc.DropdownMenuItem("Recipes", href="/vis/eval_recipes"),
                ],
                nav=True,
                in_navbar=True,
                label="Evaluation",
            ),
        ],
        brand="Bachelor Thesis",
        color="primary",
        dark=True,
    )
    return navbar