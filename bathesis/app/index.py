import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

import plotly.express as px
import pandas as pd

from bathesis.app.app import app
from bathesis.app.navbar import Navbar
from bathesis.app.vis import clustering, bidirectional, home, evaluation

# has to be initialized in order for callbacks to work
df_a, df_n, df_tfidf, best_k_a, best_k_n = [None]*5

app.layout = html.Div([
    Navbar(),
    html.Div(id='page-content')
])

# callback for changing sites
@app.callback(Output('page-content', 'children'),
              [Input('url', 'pathname')])
def display_page(pathname):
    """
    callback to chose a new page to be displayed 

    :param pathname: the path to be shown
    :returns: layout: the layout displayed in the dashboard
    """
    # update the global variables for callbacks
    global df_a
    global df_n
    global df_tfidf
    global best_k_a
    global best_k_n
    global df_subclasses_a
    global df_subclasses_n

    if pathname == '/home' or pathname=='':
        return home.vis()
    elif pathname == '/vis/kmeans_all':
        layout, df_a, df_n, df_tfidf, best_k_a, best_k_n = clustering.vis('all', 'kmeans')
        return layout
    elif pathname == '/vis/kmeans_articles':
        layout, df_a, df_n, df_tfidf, best_k_a, best_k_n = clustering.vis('articles', 'kmeans')
        return layout
    elif pathname == '/vis/kmeans_comments':
        layout, df_a, df_n, df_tfidf, best_k_a, best_k_n = clustering.vis('comments', 'kmeans')
        return layout
    elif pathname == '/vis/kmeans_recipes':
        layout, df_a, df_n, df_tfidf, best_k_a, best_k_n = clustering.vis('recipes', 'kmeans')
        return layout
    elif pathname == '/vis/aggl_all':
        layout, df_a, df_n, df_tfidf, best_k_a, best_k_n = clustering.vis('all', 'aggl')
        return layout
    elif pathname == '/vis/aggl_articles':
        layout, df_a, df_n, df_tfidf, best_k_a, best_k_n = clustering.vis('articles', 'aggl')
        return layout
    elif pathname == '/vis/aggl_comments':
        layout, df_a, df_n, df_tfidf, best_k_a, best_k_n = clustering.vis('comments', 'aggl')
        return layout
    elif pathname == '/vis/aggl_recipes':
        layout, df_a, df_n, df_tfidf, best_k_a, best_k_n = clustering.vis('recipes', 'aggl')
        return layout
    elif pathname == '/vis/bidi_all':
        layout, df_a, df_n, df_subclasses_a, df_subclasses_n, best_k_a, best_k_n = bidirectional.vis('all')
        return layout
    elif pathname == '/vis/bidi_articles':
        layout, df_a, df_n, df_subclasses_a, df_subclasses_n, best_k_a, best_k_n = bidirectional.vis('articles')
        return layout
    elif pathname == '/vis/bidi_comments':
        layout, df_a, df_n, df_subclasses_a, df_subclasses_n, best_k_a, best_k_n = bidirectional.vis('comments')
        return layout
    elif pathname == '/vis/bidi_recipes':
        layout, df_a, df_n, df_subclasses_a, df_subclasses_n, best_k_a, best_k_n = bidirectional.vis('recipes')
        return layout
    elif pathname == '/vis/eval_all':
        layout = evaluation.vis('all')
        return layout
    elif pathname == '/vis/eval_articles':
        layout = evaluation.vis('articles')
        return layout
    elif pathname == '/vis/eval_comments':
        layout = evaluation.vis('comments')
        return layout
    elif pathname == '/vis/eval_recipes':
        layout = evaluation.vis('recipes')
        return layout
    else:
        return '404'

# apparently callbacks cannot be imported - for some reason - and have to be written in the index.py

"""
Callbacks for kmeans and agg visualizations
"""

# Callback for the scatter plot of a single cluster and the bar chart of the most relevant other ones
@app.callback([
    Output('cluster-single', 'figure'),
    Output('cor-clusters', 'figure'),
    Output('single-cluster-heading', 'children')],
    [Input("cluster-select", "value"),
    Input("cluster-slider", "value")]
)
def update_single_cluster(n_cluster, num):
    """
    callback to update the visualization of single clusters

    :param n_cluster: the name of the cluster to be shown
    :param num: the number of most relevant clusters to be shown

    :returns: tuple containing three items:
       * scatter: the scatter plot of the single cluster
       * bar: the bar chart of the most relevant clusters
       * heading: the heading to be shown
    """
    if n_cluster < best_k_a:
        df = df_a[df_a['cluster']==n_cluster]
        tfidf = df_tfidf
    else:
        df = df_n[df_n['cluster']==n_cluster]
        tfidf = df_tfidf.transpose()
    tfidf = tfidf.nlargest(num,n_cluster).sort_values(by=[n_cluster], ascending=False)

    scatter = px.scatter_3d(df, x="x", y="y", z='z', color="cluster", custom_data=[df.index, df['cluster']])
    scatter.update_layout(xaxis={'title_text': None, 'showticklabels': False, 'showgrid':False, 'zeroline':False} , yaxis={'title_text': None, 'showticklabels': False, 'showgrid':False, 'zeroline':False})
    scatter.update_traces(hovertemplate='<b> %{customdata[0]} </b> <br>Cluster: %{customdata[1]} <extra></extra>')

    bar = px.bar(tfidf, x=tfidf.index, y=n_cluster)
    bar.update_traces(hovertemplate='<b> %{x} </b> <br>Tf-idf score: %{y} <extra></extra>')
    bar.update_layout(xaxis={'title_text': None, 'tickangle':45, 'type':'category'} , yaxis={'title_text': None})
    heading = "Visualization of cluster {}".format(n_cluster)
    return scatter, bar, heading

# Callback for showing the stats of a single word (tfidf and frquency in cluster)
@app.callback([
    Output('word-stats', 'figure'),
    Output('cor-words', 'figure')],
    [Input("word-select", "value"),
    Input("word-slider", "value")]
)
def update_single_word(word, slider):
    """
    callback for visualization of a single word

    :param word: the word to be visualized
    :param slider: the number of most relevant words to be shown

    :returns: tuple containing two items:
       * frequency: bar chart showing the frequency of a word within its own cluster
       * likely: bar chart showing the most likely words for it to appear with
    """
    if word in df_a.index:
        df = df_a.copy()
        df_l = df_n.copy()
        tfidf = df_tfidf.copy()
    else:
        df = df_n.copy()
        df_l = df_a.copy()
        tfidf = df_tfidf.transpose()

    cluster = df['cluster'][word]
    tfidf = tfidf[cluster] # shorten to one column to speed up looking up values
    df_word = df[df['cluster']==cluster].nlargest(20,'frequency')
    if word not in df_word.index:
        df_word = df_word.iloc[:-1].append(pd.DataFrame({'frequency':df['frequency'][word]}, index=[word]))
    colors = ['lightslategray']*len(df_word)
    colors[list(df_word.index).index(word)] = 'crimson'
    df_word['colors'] = colors

    df_l['likely'] = [df_l['score'][n]*tfidf[df_l['cluster'][n]] for n in df_l.index]
    df_l = df_l.nlargest(slider, 'likely').sort_values(by=['likely'], ascending=False)

    frequency = px.bar(df_word, x=df_word.index, y="frequency", color="colors", 
       custom_data=[df_word['cluster'], df_word['score']], 
       color_discrete_map={"crimson": "crimson", "lightslategray": "lightslategray"})
    frequency.update_traces(hovertemplate='<b> %{x} </b> <br>Frequency in cluster %{customdata[0]}: %{y} <br>Score in cluster %{customdata[0]}: %{customdata[1]} <extra></extra>')
    frequency.update_layout(xaxis={'title_text': None, 'tickangle':45} , yaxis={'title_text': None}, xaxis_categoryorder = 'total descending')
    
    likely = px.bar(df_l, x=df_l.index, y="likely", color="cluster", custom_data=[df_l['cluster']])
    likely.update_traces(hovertemplate='<b> %{x} </b> <br>Likelihood score: %{y} <br>Cluster: %{customdata} <extra></extra>')
    likely.update_layout(xaxis={'title_text': None, 'tickangle':45} , yaxis={'title_text': None})
    return frequency, likely

"""
Callbacks for bidirectional visualization
"""
@app.callback([
    Output('bidi-cluster', 'figure'),
    Output('bidi-cluster-heading', 'children')],
    [Input("bidi-select", "value")]
)
def update_bidi(n_cluster):
    if n_cluster <= best_k_a:
        df = df_a[df_a['class']==n_cluster]
        df_sub = df_subclasses_a[df_subclasses_a['class']==n_cluster]
        df = pd.concat([df, df_sub])
    else:
        df = df_n[df_n['class']==n_cluster]
        df_sub = df_subclasses_n[df_subclasses_n['class']==n_cluster]
        df = pd.concat([df, df_sub])

    scatter = px.scatter(df, x="x", y="y", color="class", custom_data=[df.index, df['class']])
    scatter.update_layout(xaxis={'title_text': None, 'showticklabels': False, 'showgrid':False, 'zeroline':False} , yaxis={'title_text': None, 'showticklabels': False, 'showgrid':False, 'zeroline':False})
    scatter.update_traces(hovertemplate='<b> %{customdata[0]} </b> <br>Cluster: %{customdata[1]} <extra></extra>', marker=dict(size=12))

    heading = "Visualization of cluster {}".format(n_cluster)

    return scatter, heading

@app.callback(
    Output('bidi-words', 'figure'),
    [Input("word-bidi-select", "value")]
)
def update_bidi(word):
    if word in df_a.index:
        df = df_a.copy()
        df_sub = df_subclasses_a.copy()
    else:
        df = df_n.copy()
        df_sub = df_subclasses_n.copy()
        
    cluster = df['class'][word]
    df = pd.concat([df[df['class']==cluster], df_sub[df_sub['class']==cluster]])

    scatter = px.scatter(df, x="x", y="y", color="class", custom_data=[df.index, df['class']])
    scatter.update_layout(xaxis={'title_text': None, 'showticklabels': False, 'showgrid':False, 'zeroline':False} , yaxis={'title_text': None, 'showticklabels': False, 'showgrid':False, 'zeroline':False})
    scatter.update_traces(hovertemplate='<b> %{customdata[0]} </b> <br>Cluster: %{customdata[1]} <extra></extra>', marker=dict(size=12))

    return scatter

if __name__ == '__main__':
    app.run_server(debug=True)