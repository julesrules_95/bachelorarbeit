def warn(*args, **kwargs):
    pass
import warnings
warnings.warn = warn

import argparse
import subprocess
from pathlib import Path

from bathesis.download import get_all_recipe_links, download
from bathesis.cleaning import update_pairs, update_pairs_all, count_pairs, clean_counts
from bathesis.kmeans import kmeans
from bathesis.clustering import test_agg
from bathesis.bidirectional import apply_bidirectional
from bathesis.evaluation import build_gold_list, run_eval
from bathesis.zipping import zip_dir, unzip_dir

def main():
    """
    Wrapper for CLI
    """
    parser = argparse.ArgumentParser(description='CLI interface for the bathesis-project.')
    subparsers = parser.add_subparsers(dest='command', help='Give some commands on what to do:')

    linker = subparsers.add_parser('updatelinks', help='Download links to recipes')
    linker.add_argument('-s', '--source', type=str, nargs='*',
                        help='The id of resources you want to get links from. Try: dkr, chk, kg, kb, kl, ttr, ern')
    linker.add_argument('--startnew', action='store_true',
                        help='Start updating links from blank dict.')

    downloader = subparsers.add_parser('download', help='Download recipes')
    downloader.add_argument('-s', '--source', type=str, nargs='*',
                        help='The id of resources you want download from. Try: dkr, chk, kg, kb, kl, ttr, ern')
    downloader.add_argument('--startnew', action='store_true',
                        help='Start downloading the resource anew.')

    pairer = subparsers.add_parser('pair', help='Find noun-adjective pairs')
    pairer.add_argument('-s', '--source', type=str, nargs='*',
                        help='The id of resources you want to extract pairs from. Try: articles, comments, recipes, all')

    counter = subparsers.add_parser('count', help='Count the frequency of all noun-adjective pairs')
    counter.add_argument('-s', '--source', type=str, nargs='*',
                        help='The id of resources for which you want to count the pairs. Try: articles, comments, recipes, all')

    cleaner = subparsers.add_parser('clean', 
                        help='Clean the counts by removing pairs that are less frequent than in the reference corpus')
    cleaner.add_argument('-s', '--source', type=str, nargs='*',
                        help='The id of resources for which you want to clean the counts. Try: articles, comments, recipes, all')

    c_kmeans = subparsers.add_parser('kmeans', help='Perform kmeans clustering on the corpus')
    c_kmeans.add_argument('-s', '--source', type=str, nargs='*',
                        help='The id of resources you want to perform kmeans on. Try: articles, comments, recipes, all')
    c_kmeans.add_argument('--extensive', action='store_true', 
                        help='Choose to do extensive clustering by using a range of numbers as possible n_cluster.')
    c_kmeans.add_argument('--supervised', action='store_true',
                        help='Choose the best number of clusters yourself instead of having it automatically chosen for you.')

    aggl = subparsers.add_parser('aggl', help='Perform agglomerative clustering.')
    aggl.add_argument('source', type=str, default='recipes',
                        help='The id of the resource you want to perform bidirectional clustering on. Try: articles, comments, recipes, all')

    c_bidi = subparsers.add_parser('bidirectional', help='Perform bidirectional clustering on the corpus.')
    c_bidi.add_argument('source', type=str, default='recipes',
                        help='The id of the resource you want to perform bidirectional clustering on. Try: articles, comments, recipes, all')

    gold = subparsers.add_parser('gold', help='Build (binary) gold standard from given csv-files.')

    evaluate = subparsers.add_parser('evaluate', help='Run the evaluation on the clustered data.')

    visualizer = subparsers.add_parser('visualize', help='Start a dashboard showing you all your results.')

    zipper = subparsers.add_parser('zip', help='Zip static files to make pushing to repo easier.')

    unzipper = subparsers.add_parser('unzip', help='Unzip static files you have downloaded from repo.')


    args = parser.parse_args()

    sources = ['dkr', 'chk', 'kg', 'kb', 'kl', 'ttr', 'ern']

    kinds = ['articles', 'comments', 'recipes']

    def make_source(argsrc):
        src = []
        for s in argsrc:
            if s in sources:
                src.append(s)
            else:
                print("Oh, {} is not a valid source! You might wanna try one of these: {}".format(s, sources))
        return src
    
    if args.command == 'updatelinks':
        if args.source:
            src = make_source(args.source)
            if src:
                get_all_recipe_links(src, args.startnew)
        else:
            get_all_recipe_links(sources, args.startnew)
    
    elif args.command == 'download':
        if args.source:
            src = make_source(args.source)
            if src:
                download(src, args.startnew)
        else:
            download(sources, args.startnew)

    elif args.command == 'pair':
        if args.source:
            src = []
            all = False
            for s in args.source:
                if s in kinds:
                    src.append(s)
                elif s == 'all':
                    all = True
                else:
                    print("Oh, {} is not a valid source! You might wanna try one of these: {}".format(s, kinds))
            if src:
                update_pairs(src, False)
            if all:
                update_pairs_all()
        else:
            update_pairs(kinds)
            update_pairs_all()

    elif args.command == 'count':
        if args.source:
            src = []
            for s in args.source:
                if s in kinds or s == 'all':
                    src.append(s)
                else:
                    print("Oh, {} is not a valid source! You might wanna try one of these: {}".format(s, kinds))
            count_pairs(src)
        else:
            count_pairs(kinds+['all'])

    elif args.command == 'clean':
        if args.source:
            src = []
            for s in args.source:
                if s in kinds or s == 'all':
                    src.append(s)
                else:
                    print("Oh, {} is not a valid source! You might wanna try one of these: {}".format(s, kinds))
            clean_counts(src)
        else:
            clean_counts(kinds+['all'])

    elif args.command == 'kmeans':
        if args.source:
            for s in args.source:
                if s in kinds or s == 'all':
                    kmeans(s, extensive=args.extensive, supervised=args.supervised)
                else:
                    print("Oh, {} is not a valid source! You might wanna try one of these: {}".format(s, kinds))
        else:
            for s in kinds+['all']:
                kmeans(s, extensive=args.extensive, supervised=args.supervised)

    elif args.command == 'aggl':
        test_agg(args.source)

    elif args.command == 'bidirectional':
        apply_bidirectional(args.source)

    elif args.command == 'gold':
        build_gold_list()

    elif args.command == 'evaluate':
        run_eval()

    elif args.command == 'visualize':
        subprocess.run(['python', str(Path(__file__).parent) + "/app/index.py"])

    elif args.command == 'zip':
        zip_dir()

    elif args.command == 'unzip':
        unzip_dir()

    else:
        parser.print_help()

def vis():
    subprocess.run(['python', str(Path(__file__).parent) + "/app/index.py"])