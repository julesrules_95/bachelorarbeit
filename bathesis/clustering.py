# SPIELWIESE FOR CLUSTERING

# this file contains nothing of impact for the thesis itself but some alternative 
# ways of clustering the data being agglomerative clustering and lda

# the results of agglomerative clustering may be stored in a subfolder and shown in the 
# dashboard but keep in mind that only kmeans is relevant for evaluation
import json
import pandas as pd

from bathesis.visualizations import plot_lda, simple_plot
from bathesis.kmeans import get_clean_data, apply_tsne, separate_word2vec, prepare_counts, add_scores, tfidf_on_counts, combine_data # only done because both use the same preprocessing -- but kmeans is still main clustering method

from pathlib import Path

import gensim
import gensim.corpora as corpora
from gensim.models import CoherenceModel

from sklearn.neighbors import NearestCentroid
from sklearn.cluster import AgglomerativeClustering

def make_doc_format(id, index='n'):
    """
    turns df into a list of lists where each list represents a noun and 
    each element in a list a corresponding adjective

    :param id: the resource to be converted to doc format
    :param index: determines whether to use nouns or adjectives

    :returns:
       tuple containing two items:
          * indices: the nouns (or adjectives) depending of the value of ``index``
          * word_list: the list of words each index cooccurs with
    """
    with open(str(Path(__file__).parent) + '/static/cleaned/{}-counts-cleaned.csv'.format(id)) as f:
        df = pd.read_csv(f, index_col=0)

    if index == 'n':
        df = df.transpose()
    indices = df.columns
    word_list = []

    for i in indices:
        words = []
        tmp = df[i][df[i]>0]
        for index, value in tmp.items():
            words = words + [index] * value
        word_list.append(words)

    return indices, word_list


def gensim_corpus(data):
    id2word = corpora.Dictionary(data)
    corpus = [id2word.doc2bow(text) for text in data]

    return id2word, corpus


def apply_agg(tokens, vocab, start, d_thr):
    """
    Function to actually apply agglomerative clustering. 
    Uses a distance threshold as an alternative approach to finding the 
    k best clusters. 

    :param tokens: the wordvectors to be reduced in dimensionality
    :param vocab: the vocab (from word2vec)
    :param start: integer denoting the start of the new clusters found
    :param d_thr: the distance threshold to use

    :returns: 
       tuple containing four items:
          * df: the dataframe containing each word, its coordinates and the cluster it was assigned to
          * k: the number of clusters found
          * centers: the coordinates of the centeroid of each cluster
          * cluster_dict: initialized dictionary for mapping the cluster number to its contents
    """
    X = apply_tsne(tokens, n_components=3)
    agg = AgglomerativeClustering(n_clusters=None, distance_threshold=d_thr, linkage="average").fit(X)
    cluster = agg.labels_
    k = agg.n_clusters_
    print(k)
    cluster = [i+start for i in cluster]
    clf = NearestCentroid().fit(X, cluster)
    centers = pd.DataFrame(clf.centroids_, index=range(start, start+k), columns=['x', 'y', 'z'])

    df = combine_data(vocab, X, cluster)
    cluster_dict = {n:0 for n in range(k)} #only initialize -- values will be set later

    return df, k, centers, cluster_dict


def test_agg(id):
    """
    Alternative clustering method. Works almost fully like kmeans but uses 
    agglomerative clustering instead of kmeans clustering as an opposing approach 
    which does not have to find the best k.

    :param id: the id of the resource to be clustered

    :returns: None
    """
    if id == 'articles':
        min_count = 2
        d_thr = 25
    elif id == 'all':
        min_count = 15
        d_thr = 2
    else: 
        min_count = 10
        d_thr = 2
    # get clusters using word2vac and kmeans
    data, pos_dict = get_clean_data(id)
    print(len(data))
    print("corpus done...")
    # if no randomness is desired, seed and only one worker is required
    vocab_n, tokens_n, vocab_a, tokens_a = separate_word2vec(data, pos_dict, min_count=min_count)
    print("word2vec done...")
    print(len(vocab_a), ", ", len(vocab_n))

    df_a, best_k_a, centers_a, cluster_dict_a = apply_agg(tokens_a, vocab_a, 0, d_thr)
    df_n, best_k_n, centers_n, cluster_dict_n = apply_agg(tokens_n, vocab_n, best_k_a, d_thr)
    cluster_dict = {**cluster_dict_a, **cluster_dict_n}
    n_clusters = best_k_a + best_k_n

    # get tfidf values to estimate most relevant clusters
    data_tfidf, cluster_dict = prepare_counts(id, df_a, df_n, cluster_dict, n_clusters)
    df_a = add_scores(df_a, centers_a, cluster_dict)
    df_n = add_scores(df_n, centers_n, cluster_dict)
    X_tfidf = tfidf_on_counts(data_tfidf).toarray()
    df_tfidf = pd.DataFrame(data=X_tfidf, index=list(range(n_clusters))[best_k_a:], columns=list(range(n_clusters))[:best_k_a])

    df_a.to_csv(str(Path(__file__).parent) + "/static/clustered/aggl/{}/df_a.csv".format(id))
    df_n.to_csv(str(Path(__file__).parent) + "/static/clustered/aggl/{}/df_n.csv".format(id))
    df_tfidf.to_csv(str(Path(__file__).parent) + "/static/clustered/aggl/{}/df_tfidf.csv".format(id))

    cluster_dict = {'kind':id, 'best_k_a': best_k_a, 'best_k_n': best_k_n}
    with open(str(Path(__file__).parent) + "/static/clustered/aggl/{}/dict.json".format(id), 'w') as f:
        json.dump(cluster_dict, f)



def test_lda(id, n_clusters=[10,20,40,80,100,130,160,200,250,300,350,400,450,500,750], kind='n'):
    vocab, words = make_doc_format(id, kind)
    print(vocab[:10], words[:10])
    id2word, corpus = gensim_corpus(words)
    print("corpus done")
    coh = []
    for i in n_clusters:
        lda_model = gensim.models.ldamodel.LdaModel(corpus=corpus,
                                            id2word=id2word,
                                            num_topics=i, 
                                            random_state=100,
                                            update_every=1,
                                            chunksize=100,
                                            passes=10,
                                            alpha='auto',
                                            eta='auto',
                                            per_word_topics=True)
        #print(lda_model.print_topics())
        #doc_lda = lda_model[corpus]

        # Compute Coherence Score
        coherence_model_lda = CoherenceModel(model=lda_model, texts=words, dictionary=id2word, coherence='c_npmi')
        coherence_lda = coherence_model_lda.get_coherence()
        print('\nCoherence Score: ', coherence_lda)
        coh.append(coherence_lda)

    simple_plot(n_clusters, coh)

    best_n = int(input("Enter best value from coherence score: "))

    lda_model = gensim.models.ldamodel.LdaModel(corpus=corpus,
                                            id2word=id2word,
                                            num_topics=best_n, 
                                            random_state=100,
                                            update_every=1,
                                            chunksize=100,
                                            passes=10,
                                            alpha='auto',
                                            eta='auto',
                                            per_word_topics=True)

    plot_lda(lda_model, corpus, id2word)