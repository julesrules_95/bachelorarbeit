import re
import time
import json
import emoji # remove them as they are irrelevant for task
import requests
import traceback
from pathlib import Path
from bs4 import BeautifulSoup as bs

class Resource:

    # pages to be scraped for german cooking and baking recipes (and drinks)
    category_identifiers = {'dkr': ['div', 'recipe-categories-cluster-list__caption'], 
                            'chk': ['h2', 'category-level-1'], 
                            'kg': ['td', None], 
                            'ttr': ['a', 'child-links__link'],
                            'ern': ['a', 'child-links__link']}

    link_identifiers = {'kl': ['a', 'm-recipe-tile__link'], 
                        'kb': ['a', 'kb-teaser-list-link'], 
                        'chk': ['a', 'bi-recipe-item'], 
                        'kg': ['div', 'teaser'], 
                        'dkr': ['a', 'teaser-search-result__title'],
                        'ttr': ['h3', 'article-teaser-body__title'],
                        'ern': ['h3', 'article-teaser-body__title']}

    textdata_identifiers = {'kl': ['p', 't-recipes-detail__cooking-description'], 
                            'kb': ['div', 'ks-preparation__step'], 
                            'chk': ['div', 'ds-box'], 
                            'kg': ['span', 'recipe-preparation__text'], 
                            'dkr': ['div', 'recipe-detail-step__body'],
                            'ttr': ['p', None],
                            'ern': ['p', None]}

    comment_ids = {'chk': ['div', 'comment-item']}
    
    comment_texts = {'chk': ['p', None]}

    page_initializer = {'dkr': '?page=0', 
                        'chk': '/s0g', 
                        'kg': '?seite=0', 
                        'kl': 'rezeptsuche.pageIndex=0.html', 
                        'kb': '?sort=create&order=desc&page=0', 
                        'ttr': '?page=0',
                        'ern': '?page=0'}

    page_indicator = {'dkr': r'(?<=\?page=)\d+', 
                    'chk': r'(?<=/s)\d+(?=g)', 
                    'kg': r'(?<=\?seite=)\d+', 
                    'kl': r'(?<=rezeptsuche\.pageIndex=)\d+(?=\.html)', 
                    'kb': r'(?<=\?sort=create&order=desc&page=)\d+', 
                    'ttr': r'(?<=\?page=)\d+',
                    'ern': r'(?<=\?page=)\d+'}

    urls = {'dkr': 'https://www.daskochrezept.de/rezeptkategorien', 
            'chk': 'https://www.chefkoch.de/rezepte/kategorien/', 
            'kg': 'https://www.kuechengoetter.de/rezepte', 
            'kl': 'https://www.kaufland.de/rezepte/rezeptsuche.html', 
            'kb': 'https://www.kochbar.de/rezepte/alle-rezepte.html', 
            'ttr': 'https://www.daskochrezept.de/magazin/tipps-und-tricks',
            'ern': 'https://www.daskochrezept.de/magazin/ernaehrung'}

    basic_urls = {'dkr': 'https://www.daskochrezept.de', 
                'chk': 'https://www.chefkoch.de', 
                'kg': 'https://www.kuechengoetter.de', 
                'kl': 'https://www.kaufland.de', 
                'kb': 'https://www.kochbar.de', 
                'ttr': 'https://www.daskochrezept.de',
                'ern': 'https://www.daskochrezept.de'}

    com_delete = ['Dein Kommentar wird gespeichert...', 
                  'Dein Kommentar wurde erfolgreich gespeichert.', 
                  'Dein Kommentar konnte nicht gespeichert werden.',
                  'Update deinen Browser um diese Internetseite korrekt angezeigt zu bekommen.',
                  'Update meinen Browser jetzt.']

    ditch_cat_chk = ['rs/s0g85/Rezepte-nach-Laendern.html', 
                     'rs/s0g32/Speisearten.html', 
                     'rs/s0g53/saisonale-Rezepte.html']
 
    def __init__(self, name):
        # setting all properties of a resource here
        self.id = name
        if name in self.category_identifiers.keys():
            self.category_identifier = self.category_identifiers[name]
        else:
            self.category_identifier = None
        self.page_init = self.page_initializer[name]
        self.page_ind = self.page_indicator[name]
        self.basic_url = self.basic_urls[name]
        self.link_identifier = self.link_identifiers[name]
        self.textdata_identifier = self.textdata_identifiers[name]
        if name in self.comment_ids.keys():
            self.comment_id = self.comment_ids[name]
            self.comment_text = self.comment_texts[name]
        else:
            self.comment_id = None
            self.comment_text = None


    def find_categories(self):
        """
        this function takes a web-adress and looks for the links of recipe-categories 
        based on the ``category_identifiers`` dict. All the found links to categories 
        are stored in the ``links-to-categories`` dict.

        :returns: None
        """
        links = []
        page = requests.get(self.urls[self.id])
        soup = bs(page.content, 'html.parser')
        categories = soup.findAll(self.category_identifier[0], class_=self.category_identifier[1])
        for cat in categories:
            if self.category_identifier[0] != 'a':
                link = cat.find('a')
            else:
                link = cat
            if link:
                links.append(link['href'])
            #else:
                #print("Uhoh, no link here o.o")
        if self.id == 'kg':
            links = [l for l in links if str.startswith(l, '/gerichttyp') or str.startswith(l, '/menuefolge')]
        elif self.id == 'chk':
            links = [l for l in links if l not in self.ditch_cat_chk]
        return set([self.basic_url + l for l in links])
        #self.links_to_categories[self.id] = set([self.basic_url + l for l in links])


    def find_links(self, url):
        """
        takes a source id and an url to find all links to individual recipes on the page

        :param url: the link of the page to get recipe-links from

        :returns: list of all recipe-links found
        """
        links = []
        page = requests.get(url)
        soup = bs(page.content, 'html.parser')
        #print(soup)
        recipes = soup.findAll(self.link_identifier[0], class_=self.link_identifier[1])
        for rec in recipes:
            if self.link_identifier[0] == 'a':
                link = rec
            else:
                link = rec.find('a')
            if link:
                links.append(link['href'].replace(self.basic_url, ''))
        return [self.basic_url + l for l in links]


    def scroll_through_pages(self, url):
        """
        takes the id of a host and the url of a page or category containing recipes on 
        multiple sites, then iterates over all these sites based on the indicator in the 
        urls stored in ``page-indicator``

        :param url: the url of the first (superior) page

        :returns: a list of all recipe-links that could be found beginning with url
        """
        # infix at chk, also only one to have indicator already in link
        # all other: add while scrolling
        page_url = url
        next_page = True
        scrolling = []  #gets set to the last found links after searching the first page which is scrolled
        i = 0
        
        try:
            #first iteration to get first page and append the page-related parts of the url
            found = self.find_links(page_url)
            add_to_links(self.id, found)
            i += 1
            if self.id != 'chk':
                page_url = page_url + self.page_init
            s = time.time()
                
            # iterating through pages
            while next_page:
                # wait half a second before next request to server to avoid errors
                s = wait_between_iter(s)

                if self.id != 'chk':
                    page_url = re.sub(self.page_ind, str(i) , page_url)
                else:
                    page_url = re.sub(self.page_ind, str(i*30) , page_url)
                print(page_url)
                found = self.find_links(page_url)
                #print(found)
                # if links on next page are exactly the same something might be wrong
                if scrolling and set(sorted(found)) == set(sorted(scrolling)):
                    print("Something's fishy here...")
                    next_page = False 
                elif found:
                    #print(found)
                    add_to_links(self.id, found)
                    i += 1
                else: 
                    print("Nothing left on page!")
                    next_page = False 
                scrolling = found.copy()
                
        except Exception as e:
            next_page = False
            print("Something went wrong: ")
            traceback.print_exc()


    def norm_text(self, texts, comment=False):
        """
        normalizes each string of a recipe and gives back all strings of one recipe or comment section 
        combined into one 

        :param texts: a list of either recipe texts, comment texts or an article-text
        :param comment: a boolean indicating if texts are comments to remove automated strings 
           from the website and emojis

        :returns: text: a string of all normalized texts (seperated by '.' by default)
        """
        text = []
        texts = [re.sub(emoji.get_emoji_regexp(), '', t) for t in texts if t not in self.com_delete]
        for t in texts:
            t = re.sub(r'\\r|\\n|^\s+|\s+$', '', t) #remove unnecessary whitespaces
            t = re.sub(r'(:|;)-?(\)|/|\()','', t) #remove smileys
            t = re.sub(r'\s{2,}|\xa0', ' ', t) #normalize multiple whitespaces to one
            t = re.sub(r'([^.!?])$', r'\1.', t) #add '.' at the end of a string if no punctuation there
            text.append(t)
        text = ' '.join([re.sub(r'\d+?\.([a-zA-ZüäöÜÄÖß´]+)', r'\1', t) for t in text])
        return text
    
    
    def download_textdata(self ,url):
        """
        gets the text-data of a page being either a recipe or, in case of ``ttr`` and ``ern`` 
        the textdata of an article. 

        :param url: the url to get the text from

        :returns: recipe: the normalized textdata of the page in one string
        """
        poss_text = None
        page = requests.get(url)
        soup = bs(page.content, 'html.parser')

        if self.id != 'chk' and self.id != 'ttr':
            poss_text = soup.findAll(self.textdata_identifier[0], class_=self.textdata_identifier[1])
        elif self.id == 'chk':
            article = soup.findAll('article', class_='ds-box')
            for art in article:
                if art.find('h2') and art.find('h2').get_text() == 'Zubereitung':
                    poss_text = [art.find(self.textdata_identifier[0], class_=self.textdata_identifier[1])]
        elif self.id == 'ttr':
            tmp = [(soup.find('div', class_='article__teaser-text'))]
            tmp = tmp + soup.findAll('div', 'content-item__content')
            pt = [pt.findAll(self.textdata_identifier[0], class_=self.textdata_identifier[1]) for pt in tmp]
            poss_text = [t for p in pt for t in p if t != None]
            
        texts = [t.getText() for t in poss_text if t.getText()]
        recipe = self.norm_text(texts)
        #print(recipe)
        return recipe

        
    def download_comments(self, url):
        """
        gets the first comment page from a recipe's page (being either 5 or 6 comments)

        :param url: the page to get comments from

        :returns: text: the comments normalized to one single string
        """
        comments = []
        comms = []
        page = requests.get(url)
        soup = bs(page.content, 'html.parser')
        poss_com = soup.findAll(self.comment_id[0], class_=self.comment_id[1])
        for com in poss_com:
            comms.append(com.findAll(self.comment_text[0], class_=self.comment_text[1]))
        comments = [c.getText() for com in comms for c in com if c.getText()]    
        text = self.norm_text(comments, True)
        #print(text)
        return text


def add_to_links(identifier, found):
    """
    helper function that opens the json containing the links and 
    adds all found links to it

    :param identifier: string containing the index where to add the links
    :param found: the found links to be added

    :returns: None
    """
    tmp = []
    links_to_recipes = load_json('links')
    if links_to_recipes[identifier]:
        tmp.append(links_to_recipes[identifier])
    tmp.append(found)
    tmp = [element for sublist in tmp for element in sublist]
    links_to_recipes[identifier] = list(set(tmp))
    with open(str(Path(__file__).parent) + '/static/corpus/links.json', 'w') as f:
        json.dump(links_to_recipes, f)
    links_to_recipes.clear()


def wait_between_iter(s):
    """
    makes sure to wait at least half a second between each request to avoid errors 
    due to non-respondant servers

    :param s: the start time of the last iteration
    :returns: s: the starting time of the next iteration
    """
    diff = time.time()-s
    # wait half a second before next request to server to avoid errors
    if diff < 0.5:
        time.sleep(0.5-diff)
    s = time.time()
    return s


def get_all_recipe_links(sources, start_new=False):
    """
    The main function to get all links. 
    
    First tries to open an existing ``link.json`` as this allows for getting links 
    of specific resources without losing the rest; if ``link.json`` doesn't exist yet, 
    starts from an empty dict. 
    Then iterates over all sources and based on whether they have categories (hardcoded 
    in ``category_identifiers``) first gets the links of the individual categories. Then 
    iterates over all pages containing recipes via ``scroll_through_pages`` and stores all 
    the found recipe-links in the ``links-to-recipes`` dict.

    After iterating over all sources and finding all possible recipes, the links get stored 
    in a json file to avoid frequent downloading of those.

    :param sources: list of ids of all sources

    :returns: None
    """
    print("update links for {}".format(sources))
    
    links_to_categories = {}

    for src in sources:
        src = Resource(src)
        if start_new:
            #if starting new with finding links clear all found links in resource
            links_to_recipes = load_json('links')
            links_to_recipes[src.id]=[]
            with open(str(Path(__file__).parent) + '/static/corpus/links.json', 'w') as f:
                json.dump(links_to_recipes, f)
            links_to_recipes.clear()
        if src.category_identifier:
            links_to_categories[src.id] = src.find_categories()
            for cat in links_to_categories[src.id]:
                src.scroll_through_pages(cat)
        else:
            src.scroll_through_pages(src.urls[src.id])


def load_json(kind):
    """
    helper function to improve readability of download function

    :param kind: the kind of textdata to load

    :returns: dic: the dict containing the contents of the possible json-file
    """
    try:
        with open(str(Path(__file__).parent) + '/static/corpus/{}.json'.format(kind)) as f:
            dic = json.load(f)
    except Exception as e:
        traceback.print_exc()
        if kind == 'links':
            print("getting links starting with blank dictionary")
        dic = {'dkr': [], 'chk': [], 'kb': [], 'kg': [], 'kl': [], 'ttr': [], 'ern': []}
    return dic
    

def clear_contents(src, kind):
    """
    helper function that removes the textdata of a resource

    :param source: the resource for which the textdata should be removed
    :param kind: specifies if the source is part of ``articles``, ``comments`` or ``recipes``
    :returns: None
    """
    src_texts = load_json(kind)
    src_texts[src.id] = [] #clear contents of resource
    with open(str(Path(__file__).parent) + '/static/corpus/{}.json'.format(kind), 'w') as f:
        json.dump(src_texts, f)


def download(sources, startnew=False):
    """
    The main function to download the text data.

    First tries to open possibly existing dicts in ``recipes.json``, ``comments.json`` which 
    contain previously downloaded textdata. This allows for downloading only data of specific 
    sources (instead of all at once) without losing any previously downloaded data. If these json 
    files don't exist, the function starts with empty dicts. 
    Afterwards it tries to open ``links.json``; if that doesn't exist either or is corrupted, it 
    downloads all links for the specifies dources anew using ``get_all_recipe_links``.
    Then it starts the actual downloading process by iterating over the urls of each source and 
    extracting all textdata of recipes and - if possible - comments.

    After getting all the data it is stored both in a txt-file which only contains the strings 
    and a json-file which is needed to allow updating the textdata of specific sources.

    :param sources: list of ids of all sources
    :param startnew: determines whether to add to existing tetxdata or start downloading anew

    :returns: None
    """
    print("download texts for {}".format(sources))
    resources = set() # used for determining which resources have been changed
    
    if not Path(str(Path(__file__).parent) + '/static/corpus/links.json').exists():
        print("getting all links...")
        get_all_recipe_links(sources)
        print("done getting links!")
    
    with open(str(Path(__file__).parent) + '/static/corpus/links.json') as f:
        links_to_recipes = json.load(f)
    # done with finding all links

    for src in sources:
        print("downloading data for {}...".format(src))
        src = Resource(src)
        s = time.time()
        n = 10000
        link_chunks = [links_to_recipes[src.id][i:i+n] for i in range(0, len(links_to_recipes[src.id]), n)]
        
        if src.id == 'ttr' or src.id == 'ern':
            if startnew:
                clear_contents(src, 'articles')

            article_texts = {}
            for chunk in link_chunks:
                article_texts = load_json('articles')
                for url in chunk:
                    try:
                        s = wait_between_iter(s)
                        print(url)
                        article_texts[src.id].append(src.download_textdata(url))
                    except Exception as e:
                        print("Oops, something went wrong: ")
                        traceback.print_exc()
                
                with open(str(Path(__file__).parent) + '/static/corpus/articles.json', 'w') as f:
                    json.dump(article_texts, f)
            resources.add('articles')
            print("number of articles downloaded: ", len(article_texts[src.id]))

        else:
            if startnew:
                clear_contents(src, 'recipes')
                if src.comment_text:
                    clear_contents(src, 'comments')

            recipe_texts = {}
            comment_texts = {}
            for chunk in link_chunks:
                recipe_texts = load_json('recipes')
                if src.comment_text:
                    comment_texts = load_json('comments')
                for url in chunk:
                    try:
                        s = wait_between_iter(s)
                        print(url)
                        recipe_texts[src.id].append(src.download_textdata(url))
                        if src.comment_text:
                            comment_texts[src.id].append(src.download_comments(url))
                    except Exception as e:
                        print("Oops, something went wrong: ")
                        traceback.print_exc()

                with open(str(Path(__file__).parent) + '/static/corpus/recipes.json', 'w') as f:
                    json.dump(recipe_texts, f)
                if src.comment_text:
                    with open(str(Path(__file__).parent) + '/static/corpus/comments.json', 'w') as f:
                        json.dump(comment_texts, f)
            resources.add('recipes')
            print("number of recipes downloaded: ", len(recipe_texts[src.id]))
            if src.comment_text:
                resources.add('comments')
                print("number of comments downloaded: ", len(comment_texts[src.id]))

    for kind in resources:
        # load json containing resources
        src_texts = load_json(kind)

        #remove duplicates
        for key in src_texts:
            src_texts[key] = list(set(src_texts[key]))
            print("number of {} in {} after removing duplicates:".format(kind, key), len(src_texts[key]))
        with open(str(Path(__file__).parent) + '/static/corpus/{}.json'.format(kind), 'w') as f:
            json.dump(src_texts, f)

        # save all recipes and comments to files
        with open(str(Path(__file__).parent) + '/static/corpus/{}.txt'.format(kind), 'w') as f:
            for src in src_texts:
                f.writelines("{} ".format(e) for e in src_texts[src])