import re
import gc
import json
import spacy
import numpy
import pandas as pd
import traceback
import subprocess
from time import time
from germalemma import GermaLemma
from spacy.matcher import Matcher
from pathlib import Path


def prepare_reference_counts():
    """
    loads the ``merged.dat`` from the bidirectional/data/permanent folder, 
    then creates a df out of it having nouns as indices and adjectives 
    as columns

    :returns: df: the dataframe of the counts
    """
    try:
        with open(str(Path(__file__).parent) + "/bidirectional/data/permanent/merged.dat") as f:
            refs = [i.strip().split(';') for i in f.readlines()[1:]]
    except Exception as e:
        traceback.print_exc()
        return

    nouns = sorted(set([e[0] for e in refs]))
    adjs = sorted(set([e[1] for e in refs]))
    array = numpy.zeros((len(nouns), len(adjs)))
    df = pd.DataFrame(array, index=nouns, columns=adjs, dtype=numpy.int32)
    for e in refs:
        df.at[e[0], e[1]] = e[2]
        #print(e)
    print("made df")
    
    #print(df)
    return df


def update_pairs(ids, all_pairs=True):
    """
    function that loops over the given sources and gets the noun-adjective 
    pairs for them 

    :param ids: the ids of the kind of resource to be analyzed
    :param all_pairs: determines whether the ``all-pairs.txt`` combining 
       all resources should also be updated

    :returns: None
    """
    sources = ["/static/corpus/{}.txt".format(f) for f in ids]
    all_list = []

    # idea here: only load once and only when updating
    try:
        nlp = spacy.load("de_core_news_lg")
    except Exception:
        subprocess.run(["python", "-m", "spacy", "download", "de_core_news_lg"])
        nlp = spacy.load("de_core_news_lg")
    lemmatizer = GermaLemma()

    matcher = Matcher(nlp.vocab)
    pattern = [{'POS':'ADJ', 'OP':'+'}, {'POS': {'IN': ['NOUN', 'PROPN']}}]
    matcher.add('pairs', None, pattern)
    regex = re.compile(r'^[^a-zA-Z0-9üäöÜÄÖß]+$')

    for id, src in zip(ids, sources):
        print("finding pairs for {}".format(id))
        pairs = get_pairs(src, nlp, matcher, lemmatizer, regex)
        with open(str(Path(__file__).parent) + "/static/cleaned/{}-pairs.txt".format(id), "w") as f:
            f.write(json.dumps(pairs))
        if all_pairs:
            all_list.append(pairs)
    
    if all_pairs:
        all_list = [l for li in all_list for l in li]
        with open(str(Path(__file__).parent) + "/static/cleaned/all-pairs.txt", "w") as f:
            f.write(json.dumps(all_list))


def update_pairs_all():
    """
    this function enables a user to only update the file ``all-pairs.txt`` 
    based on the ``..-pairs.txt`` files of the resources in the static/cleaned 
    directory
    """
    all_pairs = []
    resources = ['articles', 'comments', 'recipes']
    r_list = []

    for r in resources: 
        p = Path(str(Path(__file__).parent) + '/static/cleaned/{}-pairs.txt'.format(r))
        if p.is_file():
            r_list.append(p)

    for r in r_list:
        try:
            with open(r) as f:
                pairs = json.loads(f.read())
                all_pairs.append(pairs)
                print("done with adding {}".format(r))
        except Exception as e:
            traceback.print_exc()

    all_pairs = [p for pair in all_pairs for p in pair]
    with open(str(Path(__file__).parent) + "/static/cleaned/all-pairs.txt", "w") as f:
        f.write(json.dumps(all_pairs))
    print("successfully saved the updated ``all-pairs.txt``")


def get_pairs(fi, nlp, matcher, lemmatizer, regex):
    """
    uses spacy to find all noun chunks, tag them and save all adj-noun pairs 
    to a list as tuples of their lemma and pos-tag. E.g. 
    ``[[('blau', 'ADJ'), ('Haus', 'NOUN')], ...]``

    :param fi: the file-name of the txt-file to be processed
    :param nlp: the spacy-nlp-model
    :param matcher: the spacy-matcher to apply to noun chunks
    :param lemmatizer: the ``GermaLemma`` instance used to lemmatize
    :param regex: the regex to identify 'bad' words that will be skipped

    :returns: pairs: a list containing all pairs as lists of tuples (s.a.)
    """
    pairs = []

    try:
        with open(str(Path(__file__).parent) + fi) as f:
            text = f.read()
    except Exception as e:
        traceback.print_exc()
        return pairs

    # split into parts of 1 mio words
    n = 1000000
    text_chunks = [text[i:i+n] for i in range(0, len(text), n)]
    # compile regexes to 1) remove unnecessary punctuation and 2) reduce composition to only the last 
    # word as it is (in most cases) the most relevant one 
    punctuation = re.compile(r'(^[^a-zA-z0-9äüöÄÜÖß]+|[^a-zA-z0-9äüöÄÜÖß]+$|[\"\'\`\´])') 
    composition = re.compile(r'^(?:(?:\w+-)+)(\w+)$') # used to only keep last part of each composition

    for text in text_chunks:
        print("analyzing text-chunk number {}".format(text_chunks.index(text)+1))
        doc = nlp(text)

        for chunk in doc.noun_chunks: 
            matches = matcher(chunk)
            for match_id, start, end in matches:
                pair = []
                for i in range(start, end): 
                    # check if word contains any letters or numbers; if not, discard pair
                    if regex.match(chunk[i].text):
                        pair = []
                        break
                    elif chunk[i].pos_ == 'ADJ':
                        POS = 'ADJ'
                    else:
                        POS = 'N'
                    pair.append((lemmatizer.find_lemma(composition.sub(r'\g<1>', punctuation.sub('', chunk[i].text)), POS).lower(), POS))
                if pair:
                    pairs.append(pair)
    #print(pairs)
    return pairs


def count_pairs(ids):
    """
    creates a pandas dataframe with rows for each noun and columns for each adjective 
    where the cells contain the number of times these nouns and ajectives cooccur

    :param ids: the ids of the resources to be counted
    :returns: None
    """
    start = time()
    sources = ["/static/cleaned/{}-pairs.txt".format(f) for f in ids]
    
    for src, id in zip(sources, ids):
        pair_list = []
        try: 
            with open(str(Path(__file__).parent) + src) as f:
                pair_list = json.loads(f.read())
                print("successfully loaded {}".format(id))
        except Exception as e:
            traceback.print_exc()
            print("There is no such resource yet! You might wanna pair first!")
            return

        print("time consumed to load data: ", time()-start)
        tmp_time = time()

        #create the dataframe
        nouns = [e[0].strip() for entry in pair_list for e in entry if e[1] == 'N']
        adjectives = [e[0].strip() for entry in pair_list for e in entry if e[1] == 'ADJ']
        array_zero = numpy.zeros((len(set(nouns)), len(set(adjectives))))
        df = pd.DataFrame(array_zero, index=sorted(set(nouns)), columns=sorted(set(adjectives)), dtype=numpy.int32)

        print("initialized dataframe in {}".format(time()-tmp_time))
        tmp_time = time()

        # iterate over pairs, get adjectives and nouns
        print_n = 100000
        i = 0
        for pair in pair_list:
            i += 1
            adj = [t[0] for t in pair if t[1] == 'ADJ']
            noun = ' '.join([t[0] for t in pair if t[1] != 'ADJ'])
            
            # now increment cells for found adjectives
            for a in adj:
                df.at[noun,a] += 1
            if i%print_n == 0:
                print("counted {} pairs! Time remaining: {}".format(i, (((time()-tmp_time)/(i/(len(pair_list)-1)))-(time()-tmp_time))))

        print("time consumed to count: ", time()-tmp_time)

        # save dataframe as csv
        df.to_csv(str(Path(__file__).parent) + "/static/cleaned/{}-counts.csv".format(id), encoding='utf-8')

        print("successfully counted {}".format(id), " in: ", time()-start)


def prepare_df_cleaning(df):
    """
    Used to prepare dfs for the actual cleaning of infrequent words.
    Takes a df of counts which is transformed into a df containing 
    the sums of the rows and a df containing the sums of the columns.

    :param df: the original df of counts to be transformed

    :returns: tuple containing five items:
       * indices: the nouns of the original df
       * cols: the adjectives of the original df
       * sum_all: the sum of all counts in the df
       * sum_rows: the cumulated counts of nouns
       * sum_cols: the cumulated counts of adjectives
    """
    indices = list(df.index)
    cols = list(df.columns)
    array = df.to_numpy()
    sum_rows = numpy.sum(array, axis=1)
    sum_all = numpy.sum(sum_rows, axis=0)
    sum_rows = list(sum_rows)
    sum_cols = list(numpy.sum(array, axis=0))

    return indices, cols, sum_all, sum_rows, sum_cols


def clean_counts(ids):
    """
    Main function for cleaning resources from infrequent words due to 
    the assumption that words which are representative of the food-corpus 
    will appear more frequent in this corpus than the (general) reference 
    corpus.

    The function gets the reference-df via ``prepare_reference_counts()``, 
    prepares both the reference df and the df of a resource for cleaning 
    using ``prepare_df_cleaning()`` and finally iterates over the rows and 
    columns of the reference-df removing words that are infrequent. 
    Lastly the resource-df is stripped from all rows and cols that may now 
    be empty and saved in three formats being the ``counts-cleaned.csv``, 
    the ``pairs-cleaned.txt`` and the ``food-{}.dat`` where {} is the name 
    of the resource.

    :param ids: the ids of the resources to be cleaned. 
    """
    start = time()
    src_counts = ["/static/cleaned/{}-counts.csv".format(f) for f in ids]
    src_pairs = ["/static/cleaned/{}-pairs.txt".format(f) for f in ids]

    # the reference-counts need to be constructed anew every time as storing them takes up 
    # 10 to 25gb of space
    df_reference = prepare_reference_counts()
    
    for count, pair, id in zip(src_counts, src_pairs, ids):
        # try to get the file; if it does not exist, try to use the function to create it
        try: 
            with open(str(Path(__file__).parent) + count) as f:
                df_counts = pd.read_csv(f, index_col=0)
            with open(str(Path(__file__).parent) + pair) as f:
                pairs = json.loads(f.read())
            print("successfully loaded {}".format(id))
        except Exception as e:
            traceback.print_exc()
            print("There is no such resource yet! You might wanna count first!")
            return

        print("time consumed to load data: ", time()-start)
        tmp_time = time()

        # store dfs containing sums as variable to avoid frequent computation
        indices, cols, sum_counts, sum_counts_rows, sum_counts_cols = prepare_df_cleaning(df_counts)
        indices_ref, cols_ref, sum_ref, sum_ref_rows, sum_ref_cols = prepare_df_cleaning(df_reference)
        del df_reference
        gc.collect()
        # make set of indices and cols for reference corpus to speed up 
        indices_ref_set = set(indices_ref)
        cols_ref_set = set(cols_ref)

        # alternative approach: use number of sum of all counts as divisor to avoid simply larger sets
        # of pairs from being seen as more representative (total number instead of number of unique words)
        # --> now number of occurences of a word divided by number of occurrences of all words 

        dropped = set()
        pairs_cleaned = []
        print("counts before cleaning: ", df_counts.shape)
        print("time consumed to prepare for cleaning: ", time()-tmp_time)
        tmp_time = time()

        # remove less frequent nouns
        clean_time = time()
        for i in range(len(indices)):
            j = i+1
            if j%1000 == 0:
                print("cleaned {} nouns! Time remaining: {}".format(j, (((time()-clean_time)/(j/(len(indices)-1)))-(time()-clean_time))))
            index = indices[i]
            if index in indices_ref_set and (float(sum_ref_rows[indices_ref.index(index)])/float(sum_ref)) >= (float(sum_counts_rows[i])/float(sum_counts)):
                df_counts.drop(index, inplace=True)
                #print(index)
                dropped.add(index)
        # remove less frequent adjectives
        clean_time = time()
        for i in range(len(cols)):
            j = i+1
            if j%1000 == 0:
                print("cleaned {} adjectives! Time remaining: {}".format(j, (((time()-clean_time)/(j/(len(cols)-1)))-(time()-clean_time))))
            label = cols[i]
            if label in cols_ref_set and (float(sum_ref_cols[cols_ref.index(label)])/float(sum_ref)) >= (float(sum_counts_cols[i])/float(sum_counts)):
                df_counts.drop(label, axis=1, inplace=True)
                #print(label)
                dropped.add(label)

        print("counts before removing empty lines: ", df_counts.shape)

        # remove empty rows and columns (always zero)
        df_counts = df_counts.loc[:,(df_counts!=0).any(axis=0)]
        df_counts = df_counts.loc[(df_counts!=0).any(axis=1)]

        print("counts after cleaning: ", df_counts.shape)
        print("dropped words: ", len(dropped))
        print("time consumed: ", time()-tmp_time)
        tmp_time = time()
        df_counts.to_csv(str(Path(__file__).parent) + "/static/cleaned/{}-counts-cleaned.csv".format(id))

        #preparing counts in format for the bidirectional clustering
        df_bidi = pd.DataFrame(columns=['noun', 'adj', 'freq'])
        for index,row in df_counts.iterrows(): 
            #print(index) 
            #print(row.index) 
            for r,col in zip(row,row.index): 
                if r > 0: 
                    tmp = pd.DataFrame([[index, col, r]], columns=['noun', 'adj', 'freq']) 
                    df_bidi = pd.concat([df_bidi, tmp], ignore_index=True)
        df_bidi.to_csv(str(Path(__file__).parent) + "/bidirectional/data/permanent/food-{}.dat".format(id), 
                        sep=";", index=False)

        print("pairs before cleaning: ", len(pairs))
        for pair in pairs:
            append = True       # helper variable to only append pairs not in dropped
            for i in range(len(pair)):
                if pair[i][0] in dropped:
                    append = False
                    break
            if append:
                pairs_cleaned.append(pair)
        print("pairs after cleaning: ", len(pairs_cleaned))

        with open(str(Path(__file__).parent) + "/static/cleaned/{}-pairs-cleaned.txt".format(id), "w") as f:
            f.write(json.dumps(pairs_cleaned))

        print("Time consumed to clean data: ", time()-start)