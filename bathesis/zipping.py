import os
from os.path import basename
from pathlib import Path
from zipfile import ZipFile, ZIP_DEFLATED

def zip_dir():
    """
    Zips the static data to reduce space consumption in repository. 
    Creates one zip-file per folder such that in each subfolder of ``static`` 
    a zip containing the contents of that respecitve directory can be found.
    """
    # first zip raw corpus and cleaned data, then the preprocessed clustered data for each kind 
    for kind in ['cleaned', 'corpus']:
        with ZipFile(str(Path(__file__).parent)+'/static/{}/{}.zip'.format(kind, kind), 'w', compression=ZIP_DEFLATED) as zipObj:
            for folderName, subfolders, filenames in os.walk(str(Path(__file__).parent)+'/static/{}'.format(kind)):
                for filename in filenames:
                    if filename not in ['.DS_Store', '{}.zip'.format(kind)]:
                        filePath = os.path.join(folderName, filename)
                        zipObj.write(filePath, basename(filePath))
                    print("done with {}".format(filename))
    for kind in ['clustered', 'evaluation']:
        with ZipFile(str(Path(__file__).parent)+'/static/{}/{}.zip'.format(kind,kind), 'w', compression=ZIP_DEFLATED) as zipObj:
            for folderName, subfolders, filenames in os.walk(str(Path(__file__).parent)+'/static/{}'.format(kind)):
                dirname = folderName.replace(str(Path(__file__).parent)+'/static/{}/'.format(kind),'')
                for filename in filenames:
                    if filename not in ['.DS_Store', '{}.zip'.format(kind)]:
                        filePath = os.path.join(folderName, filename)
                        arcname = os.path.join(dirname, filename)
                        zipObj.write(filePath, arcname)
                    print("done with {}".format(filename))

def unzip_dir():
    """
    Unzips all zip-files in repo in their respective folder.
    """
    # first extract raw corpus and cleaned data, then the preprocessed clustered data 
    for kind in ['corpus', 'cleaned', 'clustered', 'evaluation']:
        zip_path = str(Path(__file__).parent)+'/static/{}'.format(kind)
        with ZipFile(zip_path + '/{}.zip'.format(kind), 'r') as zipObj:
            zipObj.extractall(zip_path)