=========================
Bachelor-Thesis
=========================

This repository contains all the code that was used for the bachelor thesis. 
To make things easier poetry was used to manage dependencies (instead of a 
virtual environment + requirements.txt).

In this readme you will find a small changelog and instructions on how to get the 
project to work.

Starting up poetry
------------------------
As poetry is used to manage the dependencies etc. of this project you will need 
to have poetry installed as well. To do so simply use::
   
   pip install --user poetry 

Then, check if your installation has been successful via::
   
   poetry --version

If you get an error like: ``zsh: command not found: poetry`` try one of the following:

1. if using zsh: add ``export PATH="$HOME/.poetry/bin:$PATH"`` to your ~/.zprofile or 
   if that does not exist to your ~/.zshrc 
2. if using bash: add ``export PATH="$HOME/.poetry/bin:$PATH"`` to your ~/.bash_profile 
   or if that does not exist to your ~/.bashrc
    
Afterwards run ``poetry --version`` again to check if poetry now works. 

Now that you've got poetry to work - which by the way is a great alternative to pipenv - you 
will need to install all the dependencies and libraries needed for this project. To do so simply 
use::
   
   poetry install 

or if you don't want the development dependencies::

   poetry install --no-dev

To develop inside the virtual environment created by poetry use::
   
   poetry shell
